package com.extra4it.adsapp.ui.advertisment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.SelectedAdResponse;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.rating.RatingDialog;
import com.extra4it.adsapp.ui.report.ReportActivity;
import com.extra4it.adsapp.ui.user.UserActivity;
import com.extra4it.adsapp.utils.AppConstants;
import com.extra4it.adsapp.utils.Fonts;
import com.github.ybq.android.spinkit.SpinKitView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class AdvActivity extends BaseActivity implements AdvMvpView, CommentAdapter.CommentCallBack, RatingDialog.RateCallBack {
    // injection
    @Inject
    AdvMvpPresenter<AdvMvpView> presenter;
    //Widgets
    @BindView(R.id.img_banner)
    ImageView imgBanner;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_first)
    ImageView imgFirst;
    @BindView(R.id.img_sec)
    ImageView imgSec;
    @BindView(R.id.img_third)
    ImageView imgThird;
    @BindView(R.id.tv_ad_title)
    TextView tvAdTitle;
    @BindView(R.id.img_fav)
    ImageView imgFav;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_ad_details)
    TextView tvAdDetails;
    @BindView(R.id.tv_ad_details_description)
    TextView tvAdDetailsDescription;
    @BindView(R.id.btn_show_contact)
    Button btnShowContact;
    @BindView(R.id.tv_likes)
    TextView tvLikes;
    @BindView(R.id.img_report)
    ImageView imgReport;
    @BindView(R.id.img_rate)
    ImageView imgRate;
    @BindView(R.id.tv_speech)
    TextView tvSpeech;
    @BindView(R.id.ln_likes)
    LinearLayout lnLikes;
    @BindView(R.id.ln_speech)
    LinearLayout lnSpeech;
    BottomSheetBehavior sheetBehavior;
    @BindView(R.id.ln_communication)
    LinearLayout lnCommunication;
    @BindView(R.id.bottom_sheet_comment)
    LinearLayout bottomSheetComment;
    @BindView(R.id.et_comment)
    EditText etComment;
    @BindView(R.id.img_send_comment)
    ImageView imgSendComment;
    @BindView(R.id.rv_comment)
    RecyclerView rvComment;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;
    @BindView(R.id.rb_ad)
    RatingBar rbAd;
    @BindView(R.id.ad_container)
    LinearLayout adContainer;

    private SelectedAdResponse body;
    private boolean selected = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adv);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize click listener
        onItemClicked();
        // initialize Fonts
        setupFonts();
        // hide statusBar
        hideStatusBar();
        // initialize comment bottomSheet
        sheetBehavior = BottomSheetBehavior.from(bottomSheetComment);

        // get intent data
        if (getIntent() != null) {
            presenter.loadAdData(progressBar, getIntent().getIntExtra("adId", 0));
        }

    }

    @Override
    protected void onItemClicked() {
        imgReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AdvActivity.this, ReportActivity.class);
                intent.putExtra("adId", body.getData().getAd().getId());
                startActivity(intent);
                overridePendingTransition(R.anim.push_up_out, R.anim.fade_out);
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });

        imgRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RatingDialog ratingDialog = new RatingDialog();
                ratingDialog.show(getSupportFragmentManager(), "ratingDialog");
            }
        });

        lnSpeech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });

        lnLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.addLike(view, body.getData().getAd().getId());
            }
        });
        tvUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(AdvActivity.this, UserActivity.class);
            }
        });

        imgFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.addFav(adContainer, body.getData().getAd().getId());
            }
        });

        imgSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.addComment(bottomSheetComment, body.getData().getAd().getId(), etComment);
            }
        });

    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvSpeech, tvLikes, tvAdDetails, tvAdDetailsDescription, tvAddress, tvAdTitle, tvDate, tvLocation, tvUserName}, this, 1);
        Fonts.setFont(new Button[]{btnShowContact}, this, 1);
        Fonts.setFont(new EditText[]{etComment}, this, 1);
    }

    @Override
    public void onCommentClicked(View view, int position) {
        openActivity(this, UserActivity.class);
    }

    @Override
    public void showAdsData(Response<SelectedAdResponse> response) {
        body = response.body();
        // user Data
        tvUserName.setText(body.getData().getAd().getUser().get(0).getName());
        // Ad Data
        tvDate.setText(body.getData().getAd().getCreated_at());
        tvLocation.setText(body.getData().getAd().getAddress());
        tvAdTitle.setText(body.getData().getAd().getTitle());
        tvAdDetailsDescription.setText(body.getData().getAd().getContent());
        tvSpeech.setText(String.valueOf(body.getData().getCommentsCount()));
        tvLikes.setText(String.valueOf(body.getData().getLiksCount()));
        rbAd.setRating(body.getData().getAvgRating());
        Glide.with(this).load(AppConstants.BASE_IMAGE_URL + body.getData().getAd().getImg()).into(imgBanner);
        Glide.with(this).load(AppConstants.BASE_IMAGE_URL + body.getData().getAd().getImg1()).into(imgFirst);
        Glide.with(this).load(AppConstants.BASE_IMAGE_URL + body.getData().getAd().getImg2()).into(imgSec);
        Glide.with(this).load(AppConstants.BASE_IMAGE_URL + body.getData().getAd().getImg3()).into(imgThird);
        // CommentData
        // initialize comment recyclerView
        rvComment.setLayoutManager(new LinearLayoutManager(this));
        CommentAdapter commentAdapter = new CommentAdapter(this, body.getData().getComments());
        commentAdapter.setCommentCallBack(this);
        rvComment.setAdapter(commentAdapter);
    }

    @Override
    public void updateView(int status) {
        if (status == 1) {
            imgFav.setBackground(getResources().getDrawable(R.drawable.fav_fill));
        } else {
            imgFav.setBackground(getResources().getDrawable(R.drawable.favourite));

        }
    }

    @Override
    public void clearComment() {
        etComment.setText("");
    }

    @Override
    public void onBackPressed() {
        if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRateClicked(int rate) {
        presenter.addRating(adContainer, body.getData().getAd().getId(), rate);
    }
}
