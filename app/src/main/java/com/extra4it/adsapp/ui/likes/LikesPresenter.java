package com.extra4it.adsapp.ui.likes;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class LikesPresenter<V extends LikesMvpView> extends BasePresenter<V>
        implements LikesMvpPresenter<V> {

    @Inject
    public LikesPresenter(DataManager dataManager) {
        super(dataManager);
    }
}
