package com.extra4it.adsapp.ui.search;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.SectionResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SubSectionAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;


    private Context context;
    private List<SectionResponse.Data> subSectionList = new ArrayList<>();
    private LayoutInflater inflater;
    private int rowIndex = -1;
    private SubSectionCallBack subSectionCallBack;

    public SubSectionAdapter(Context context, List<SectionResponse.Data> subSectionList) {
        this.context = context;
        this.subSectionList = subSectionList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_sections, parent, false);
                return new SubSectionViewHolder(normalView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {

        if (subSectionList != null && subSectionList.size() > 0) {
            return subSectionList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (subSectionList != null && subSectionList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public void setSubSectionCallBack(SubSectionCallBack subSectionCallBack) {
        this.subSectionCallBack = subSectionCallBack;
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class SubSectionViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_section_name)
        TextView tvSectionName;

        public SubSectionViewHolder(View itemView) {
            super(itemView);
            tvSectionName.setOnClickListener(this);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvSectionName}, context, 1);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            tvSectionName.setText(subSectionList.get(position).getSub_name());
            if (rowIndex == position) {
                tvSectionName.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                tvSectionName.setTextColor(context.getResources().getColor(R.color.colorWhite));
            } else {
                tvSectionName.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                tvSectionName.setTextColor(context.getResources().getColor(R.color.colorBlack));
            }
        }

        @Override
        public void onClick(View view) {
            rowIndex = getAdapterPosition();
            notifyDataSetChanged();
            if (subSectionCallBack != null) {
                subSectionCallBack.onSubSectionClick(rowIndex);
            }
        }
    }

    public interface SubSectionCallBack {
        void onSubSectionClick(int position);
    }
}
