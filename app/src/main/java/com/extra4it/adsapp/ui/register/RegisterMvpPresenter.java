package com.extra4it.adsapp.ui.register;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface RegisterMvpPresenter<V extends RegisterMvpView> extends MvpPresenter<V> {

    void onConfirmClicked(ProgressBar progressBar, View view,
                          EditText etUserName, EditText etPhone, EditText etMail,
                          EditText etPassword, EditText etConfirmPassword,EditText etCity, int cityId);

}
