package com.extra4it.adsapp.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.main.MainActivity;
import com.extra4it.adsapp.ui.register.RegisterActivity;
import com.extra4it.adsapp.ui.retrievePassword.RetrievePasswordActivity;
import com.extra4it.adsapp.utils.Fonts;
import com.github.ybq.android.spinkit.SpinKitView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginMvpView {
    // injection
    @Inject
    LoginMvpPresenter<LoginMvpView> presenter;
    // widgets
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.et_user_name)
    EditText etUserName;
    @BindView(R.id.tl_user_name)
    TextInputLayout tlUserName;
    @BindView(R.id.et_user_password)
    EditText etUserPassword;
    @BindView(R.id.tl_user_password)
    TextInputLayout tlUserPassword;
    @BindView(R.id.tv_forget_password)
    TextView tvForgetPassword;
    @BindView(R.id.tv_retrieve_password)
    TextView tvRetrievePassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.tv_hav_no_account)
    TextView tvHavNoAccount;
    @BindView(R.id.tv_new_register)
    TextView tvNewRegister;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;
    @BindView(R.id.login_container)
    RelativeLayout loginContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // change statusBarColor
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // initialize item click listener
        onItemClicked();
        // initialize fonts
        setupFonts();
    }

    @Override
    protected void onItemClicked() {
        tvNewRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNewRegisterClicked();
            }
        });
        tvRetrievePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onRetrievePasswordClicked();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onLoginButtonClicked(progressBar, loginContainer, etUserName, etUserPassword);
            }
        });

    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvForgetPassword, tvHavNoAccount, tvLogin, tvNewRegister, tvRetrievePassword}, this, 1);
        Fonts.setFont(new Button[]{btnLogin}, this, 1);
        Fonts.setFont(new TextInputLayout[]{tlUserName, tlUserPassword}, this, 1);
        Fonts.setFont(new EditText[]{etUserName, etUserPassword}, this, 1);
    }

    @Override
    public void openRegisterActivity() {

        openActivity(LoginActivity.this, RegisterActivity.class);
    }

    @Override
    public void openRetrievePasswordActivity() {
        openActivity(LoginActivity.this, RetrievePasswordActivity.class);
    }

    @Override
    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
