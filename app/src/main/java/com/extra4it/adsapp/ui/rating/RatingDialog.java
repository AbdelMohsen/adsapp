package com.extra4it.adsapp.ui.rating;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RatingDialog extends BaseDialog implements RatingMvpView {
    @Inject
    RatingMvpPresenter<RatingMvpView> presenter;
    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.rb_rating)
    RatingBar rbRating;
    @BindView(R.id.btn_send)
    Button btnSend;
    Unbinder unbinder;

    RateCallBack rateCallBack;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_rating, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setup(View view) {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
    }

    @Override
    protected void setupFont() {

    }

    @Override
    protected void onItemClick() {
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onCloseClicked();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rateCallBack != null) {
                    presenter.onCloseClicked();
                    rateCallBack.onRateClicked((int) rbRating.getRating());
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RateCallBack) {
            rateCallBack = (RateCallBack) context;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return fullScreenDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void closeDialog() {
        dismiss();
    }

    public interface RateCallBack {
        void onRateClicked(int rate);
    }
}
