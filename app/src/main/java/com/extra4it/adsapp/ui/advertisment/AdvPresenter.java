package com.extra4it.adsapp.ui.advertisment;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.DefaultResponse;
import com.extra4it.adsapp.data.network.model.SelectedAdResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.Validation;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdvPresenter<V extends AdvMvpView> extends BasePresenter<V>
        implements AdvMvpPresenter<V> {


    @Inject
    public AdvPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void loadAdData(final ProgressBar progressBar, int adId) {
        getMvpView().showLoading(progressBar);
        getDataManager().getApiHelper().getAdData("Bearer " + getDataManager().getAccessToken(), adId)
                .enqueue(new Callback<SelectedAdResponse>() {
                    @Override
                    public void onResponse(Call<SelectedAdResponse> call, Response<SelectedAdResponse> response) {
                        getMvpView().hideLoading(progressBar);
                        if (response.body().getStatus() == true) {
                            getMvpView().showAdsData(response);
                            getMvpView().updateView(response.body().getData().getUserfavorite());
                        }
                    }

                    @Override
                    public void onFailure(Call<SelectedAdResponse> call, Throwable t) {
                        getMvpView().hideLoading(progressBar);
                    }
                });
    }

    @Override
    public void addRating(final View view, int adId, int rate) {
        getDataManager().getApiHelper().addRate("Bearer " + getDataManager().getAccessToken(), adId, rate)
                .enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<DefaultResponse> call, @NonNull Response<DefaultResponse> response) {
                        getMvpView().showSnackBar(view, response.body().getMsg());
                    }

                    @Override
                    public void onFailure(@NonNull Call<DefaultResponse> call, @NonNull Throwable t) {

                    }
                });
    }

    @Override
    public void addFav(final View view, int adId) {
        getDataManager().getApiHelper().addFav("Bearer " + getDataManager().getAccessToken(), adId)
                .enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        getMvpView().showSnackBar(view, response.body().getMsg());

                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {

                    }
                });
    }

    @Override
    public void addLike(final View view, int adId) {
        getDataManager().getApiHelper().addLike("Bearer " + getDataManager().getAccessToken(), adId)
                .enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        getMvpView().showSnackBar(view, response.body().getMsg());

                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {

                    }
                });
    }

    @Override
    public void addComment(final View view, int adId, EditText etComment) {
        if (Validation.validate(getMvpView().getContext(), new EditText[]{etComment})) {
            getDataManager().getApiHelper().addComment("Bearer " + getDataManager().getAccessToken(), adId, etComment.getText().toString())
                    .enqueue(new Callback<DefaultResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<DefaultResponse> call, @NonNull Response<DefaultResponse> response) {
                            if (response.body() != null) {
                                Log.e("xxx", "response body");
                                getMvpView().showMessage(view, response.body().getMsg());
                                getMvpView().clearComment();
                            } else if (response.errorBody() != null) {
                                Log.e("xxx", "response error body");
                            }
                            getMvpView().hideKeyboard();
                        }

                        @Override
                        public void onFailure(@NonNull Call<DefaultResponse> call, @NonNull Throwable t) {

                        }
                    });
        }
    }
}
