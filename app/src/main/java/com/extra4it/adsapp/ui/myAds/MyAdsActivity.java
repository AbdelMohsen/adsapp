package com.extra4it.adsapp.ui.myAds;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.advertisment.AdvActivity;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class MyAdsActivity extends BaseActivity implements MyAdsMvpView, MyAdsAdapter.BrandClickCallBack {
    @Inject
    MyAdsPresenter<MyAdsMvpView> presenter;
    @BindView(R.id.tv_my_ads)
    TextView tvMyAds;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.rv_my_ads)
    RecyclerView rvMyAds;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ads);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize clickListener
        onItemClicked();
        // initialize fonts
        setupFonts();
        // change status bar color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        presenter.getUserAds(progressBar);
    }

    @Override
    protected void onItemClicked() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvMyAds}, this, 2);
    }

    @Override
    public void onBrandClicked(int position) {
        openActivity(this, AdvActivity.class);
    }

    @Override
    public void loadUserAds(Response<BrandResponse> response) {
        // initialize recyclerView
        List<BrandResponse.Data> adList = response.body().getData();
        rvMyAds.setLayoutManager(new LinearLayoutManager(this));
        MyAdsAdapter myAdsAdapter = new MyAdsAdapter(this, adList);
        myAdsAdapter.setBrandCallBack(this);
        rvMyAds.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(this, R.anim.item_anim));
        rvMyAds.setAdapter(myAdsAdapter);
    }
}
