package com.extra4it.adsapp.ui.fav;

import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.base.MvpView;

import retrofit2.Response;

public interface FavMvpView extends MvpView {
    void showUserFavData(Response<BrandResponse> response);
}
