package com.extra4it.adsapp.ui.register;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.ui.conditions.ConditionsActivity;
import com.extra4it.adsapp.ui.login.LoginActivity;
import com.extra4it.adsapp.ui.register.city.CityDialog;
import com.extra4it.adsapp.utils.Fonts;
import com.extra4it.adsapp.utils.PickImage;
import com.extra4it.adsapp.utils.RunTimePermission;
import com.github.ybq.android.spinkit.SpinKitView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.extra4it.adsapp.utils.AppConstants.CAMERA_REQUEST_CODE;
import static com.extra4it.adsapp.utils.AppConstants.IMAGE_REQUEST_CODE;

public class RegisterActivity extends BaseActivity implements RegisterMvpView, CityDialog.onCityCallBack {
    // injection
    @Inject
    RegisterMvpPresenter<RegisterMvpView> presenter;
    @Inject
    RunTimePermission permission;
    @Inject
    PickImage pickImage;
    // widgets
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.tv_new_register)
    TextView tvNewRegister;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_avatar)
    RelativeLayout imgAvatar;
    @BindView(R.id.et_user_name)
    EditText etUserName;
    @BindView(R.id.tl_user_name)
    TextInputLayout tlUserName;
    @BindView(R.id.et_user_phone)
    EditText etUserPhone;
    @BindView(R.id.tl_user_phone)
    TextInputLayout tlUserPhone;
    @BindView(R.id.et_user_mail)
    EditText etUserMail;
    @BindView(R.id.tl_user_mail)
    TextInputLayout tlUserMail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.tl_password)
    TextInputLayout tlPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.tl_confirm_password)
    TextInputLayout tlConfirmPassword;
    @BindView(R.id.tv_condition_status)
    TextView tvConditionStatus;
    @BindView(R.id.tv_condition)
    TextView tvCondition;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;
    @BindView(R.id.register_container)
    ConstraintLayout registerContainer;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.et_user_city)
    EditText etUserCity;
    @BindView(R.id.tl_user_city)
    TextInputLayout tlUserCity;

    private int cityId = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize item click listener
        onItemClicked();
        // initialize Fonts
        setupFonts();
        // change status bar
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));

    }

    @Override
    protected void onItemClicked() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });

        tvCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(RegisterActivity.this, ConditionsActivity.class);
            }
        });

        imgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                permission.selectImage();
            }
        });
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onConfirmClicked(progressBar, registerContainer, etUserName, etUserPhone,
                        etUserMail, etPassword, etConfirmPassword, etUserCity, cityId);
            }
        });

        etUserCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CityDialog().show(getSupportFragmentManager(), "CityDialog");
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvCondition, tvConditionStatus, tvConfirm, tvNewRegister}, this, 1);
        Fonts.setFont(new EditText[]{etUserCity, etConfirmPassword, etPassword, etUserMail, etUserName, etUserPhone}, this, 1);
        Fonts.setFont(new TextInputLayout[]{tlUserCity, tlConfirmPassword, tlPassword, tlUserMail, tlUserName, tlUserPhone}, this, 1);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == IMAGE_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    pickImage.openGallery();
                }
            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {

            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    pickImage.openCamera();
                }

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        pickImage.onActivityResult((BasePresenter) presenter, requestCode, resultCode, data);
    }

    @Override
    public void loadUserAvatar(Bitmap bitmap) {
        imgProfile.setImageBitmap(bitmap);
    }

    @Override
    public void openLoginActivity() {
        openActivity(this, LoginActivity.class);
    }

    @Override
    public void onCityClicked(String cityName, int cityId) {
        etUserCity.setText(cityName);
        this.cityId = cityId;
    }
}
