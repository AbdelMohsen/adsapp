package com.extra4it.adsapp.ui.changePassword;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.DefaultResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.Validation;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordPresenter<V extends ChangePasswordMvpView> extends BasePresenter<V>
        implements ChangePasswordMvpPresenter<V> {

    @Inject
    public ChangePasswordPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onChangePassword(final ProgressBar progressBar, final View view, EditText etNewPassword, EditText etConfirmNewPassword, EditText etOldPassword) {
        if (Validation.validate(getMvpView().getContext(), new EditText[]{etNewPassword, etConfirmNewPassword, etOldPassword})) {
            if (Validation.isPasswordMatch(getMvpView().getContext(), etNewPassword, etConfirmNewPassword)) {
                getMvpView().showLoading(progressBar);
                getDataManager().getApiHelper().changePassword("Bearer " + getDataManager().getAccessToken(),
                        etOldPassword.getText().toString(), etNewPassword.getText().toString())
                        .enqueue(new Callback<DefaultResponse>() {
                            @Override
                            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                                if (response.body() != null) {
                                    getMvpView().showMessage(view, response.body().getMsg());
                                }
                                getMvpView().hideLoading(progressBar);
                            }

                            @Override
                            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                                getMvpView().hideLoading(progressBar);
                            }
                        });
            }
        }
    }
}
