package com.extra4it.adsapp.ui.bankAccount;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface BankAccountMvpPresenter<V extends BankAccountMvpView> extends MvpPresenter<V> {

    void getBankAccounts();
}
