package com.extra4it.adsapp.ui.searchResult;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class SearchResultPresenter<V extends SearchResultMvpView> extends BasePresenter<V>
        implements SearchResultMvpPresenter<V> {


    @Inject
    public SearchResultPresenter(DataManager dataManager) {
        super(dataManager);
    }
}
