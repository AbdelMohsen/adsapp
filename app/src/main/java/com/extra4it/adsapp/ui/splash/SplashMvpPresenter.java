package com.extra4it.adsapp.ui.splash;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface SplashMvpPresenter<V extends SplashMvpView> extends MvpPresenter<V> {
    void onOpenDecideActivity();
}
