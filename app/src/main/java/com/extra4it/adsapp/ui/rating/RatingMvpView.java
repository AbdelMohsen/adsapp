package com.extra4it.adsapp.ui.rating;

import com.extra4it.adsapp.ui.base.MvpView;

public interface RatingMvpView extends MvpView {
    void closeDialog();
}
