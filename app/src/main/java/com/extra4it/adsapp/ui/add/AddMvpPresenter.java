package com.extra4it.adsapp.ui.add;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface AddMvpPresenter<V extends AddMvpView> extends MvpPresenter<V> {
}
