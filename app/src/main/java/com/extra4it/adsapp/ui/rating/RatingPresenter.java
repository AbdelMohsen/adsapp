package com.extra4it.adsapp.ui.rating;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class RatingPresenter<V extends RatingMvpView> extends BasePresenter<V>
        implements RatingMvpPresenter<V> {

    @Inject
    public RatingPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onCloseClicked() {
        getMvpView().closeDialog();
    }


}
