package com.extra4it.adsapp.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.HomeTagsResponse;
import com.extra4it.adsapp.data.network.model.SectionResponse;
import com.extra4it.adsapp.ui.base.BaseFragment;
import com.extra4it.adsapp.ui.brand.BrandActivity;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

public class HomeFragment extends BaseFragment implements HomeMvpView, HomeTagsAdapter.TagSelected, HomeContentAdapter.HomeContentCallBack {
    @Inject
    HomeMvpPresenter<HomeMvpView> presenter;
    @BindView(R.id.rv_home_tags)
    RecyclerView rvHomeTags;
    Unbinder unbinder;
    @BindView(R.id.rv_home_result_content)
    RecyclerView rvHomeResultContent;
    @BindView(R.id.refresh_home_content)
    SwipeRefreshLayout refreshHomeContent;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;
    private List<HomeTagsResponse.Data> homeTags;
    private List<SectionResponse.Data> subSectionList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onItemClicked() {
        refreshHomeContent.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Load Data
            }
        });

        // on Data loaded
        refreshHomeContent.setRefreshing(false);
    }

    @Override
    protected void setup(View view) {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);

        // initialize home tags
        presenter.loadHomeTags();

        // initialize home content

    }

    @Override
    protected void setupFonts() {

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onTagSelected(int position) {
        presenter.loadSubSections(progressBar, homeTags.get(position).getId());
    }

    @Override
    public void onSubSectionsClicked(int position) {
        Log.e("xxx", "" + position);
        Intent intent = new Intent(getActivity(), BrandActivity.class);
        intent.putExtra("subSectionId", subSectionList.get(position).getId());
        startActivity(intent);
        getBaseActivity().overridePendingTransition(R.anim.push_up_out, R.anim.fade_out);
    }


    @Override
    public void showHomeTags(Response<HomeTagsResponse> response) {
        homeTags = response.body().getData();
        if (getActivity() != null) {
            rvHomeTags.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,
                    false));
            HomeTagsAdapter homeTagsAdapter = new HomeTagsAdapter(getActivity(), homeTags);
            rvHomeTags.setAdapter(homeTagsAdapter);
            homeTagsAdapter.setOnTagSelectedListener(this);
            // initialize SubSection Data
            presenter.loadSubSections(progressBar, homeTags.get(0).getId());
        }

    }

    @Override
    public void showSubSections(Response<SectionResponse> response) {
        subSectionList = response.body().getData();
        rvHomeResultContent.setLayoutManager(new GridLayoutManager(getActivity(), 2, LinearLayoutManager.VERTICAL, false));
        HomeContentAdapter homeContentAdapter = new HomeContentAdapter(getActivity(), subSectionList);
        rvHomeResultContent.setAdapter(homeContentAdapter);
        homeContentAdapter.setHomeContentClicked(this);

    }
}
