package com.extra4it.adsapp.ui.searchResult;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.SearchResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SearchResultAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private Context context;
    private List<SearchResponse> searchResult = new ArrayList<>();
    private LayoutInflater inflater;
    private BrandClickCallBack brandCallBack;

    public SearchResultAdapter(Context context, List<SearchResponse> searchResult) {
        this.context = context;
        this.searchResult = searchResult;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_brand, parent, false);
                return new SearchResultViewHolder(normalView);
            case VIEW_TYPE_EMPTY:

            default:
                return new SearchResultViewHolder(inflater.inflate(R.layout.row_brand, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    @Override
    public int getItemViewType(int position) {
//        if (brandList != null && brandList.size() > 0) {
//            return VIEW_TYPE_NORMAL;
//        } else {
//            return VIEW_TYPE_EMPTY;
//        }
        return 1;
    }

    public void setBrandCallBack(BrandClickCallBack brandCallBack) {
        this.brandCallBack = brandCallBack;
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class SearchResultViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.img_brand)
        ImageView imgBrand;
        @BindView(R.id.tv_ad_description)
        TextView tvAdDescription;
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.tv_date)
        TextView tvDate;

        public SearchResultViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvDate, tvUserName}, context, 1);
            Fonts.setFont(new TextView[]{tvAdDescription}, context, 2);

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
        }

        @Override
        public void onClick(View view) {
            if (brandCallBack != null) {
                brandCallBack.onBrandClicked(getAdapterPosition());
            }
        }
    }

    public interface BrandClickCallBack {
        void onBrandClicked(int position);
    }
}
