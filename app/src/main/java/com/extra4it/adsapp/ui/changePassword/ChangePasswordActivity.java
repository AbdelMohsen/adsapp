package com.extra4it.adsapp.ui.changePassword;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;
import com.github.ybq.android.spinkit.SpinKitView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePasswordActivity extends BaseActivity implements ChangePasswordMvpView {
    @Inject
    ChangePasswordMvpPresenter<ChangePasswordMvpView> presenter;
    @BindView(R.id.tv_change_password)
    TextView tvChangePassword;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_new_password)
    TextView tvNewPassword;
    @BindView(R.id.et_new_password)
    EditText etNewPassword;
    @BindView(R.id.tv_confirm_password)
    TextView tvConfirmPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.tv_enter_old_password)
    TextView tvEnterOldPassword;
    @BindView(R.id.tv_old_password)
    TextView tvOldPassword;
    @BindView(R.id.et_old_password)
    EditText etOldPassword;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;
    @BindView(R.id.changePassword_container)
    RelativeLayout changePasswordContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);

        // initialize clickListener
        onItemClicked();
        // initialize Fonts
        setupFonts();
        // change StatusBar
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
    }

    @Override
    protected void onItemClicked() {
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onChangePassword(progressBar, changePasswordContainer, etNewPassword, etConfirmPassword, etOldPassword);
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvChangePassword, tvEnterOldPassword}, this, 2);
        Fonts.setFont(new TextView[]{tvConfirmPassword, tvNewPassword, tvOldPassword}, this, 1);
        Fonts.setFont(new EditText[]{etConfirmPassword, etNewPassword, etOldPassword}, this, 1);
        Fonts.setFont(new Button[]{btnConfirm}, this, 1);
    }
}
