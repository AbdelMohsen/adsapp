package com.extra4it.adsapp.ui.report;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.DefaultResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.Validation;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportPresenter<V extends ReportMvpView> extends BasePresenter<V>
        implements ReportMvpPresenter<V> {

    @Inject
    public ReportPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void addReport(final ProgressBar progressBar, final View view, EditText etReport, int adId) {
        getMvpView().showLoading(progressBar);
        if (Validation.validate(getMvpView().getContext(), new EditText[]{etReport})) {
            getDataManager().getApiHelper().addReport("Bearer " + getDataManager().getAccessToken(), adId, etReport.getText().toString())
                    .enqueue(new Callback<DefaultResponse>() {
                        @Override
                        public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                            getMvpView().showMessage(view, response.body().getMsg());
                            getMvpView().hideLoading(progressBar);
                            getMvpView().clearReportData();
                            getMvpView().hideKeyboard();
                        }

                        @Override
                        public void onFailure(Call<DefaultResponse> call, Throwable t) {
                            getMvpView().hideLoading(progressBar);

                        }
                    });
        }
    }
}
