package com.extra4it.adsapp.ui.conditions;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface ConditionsMvpPresenter<V extends ConditionsMvpView> extends MvpPresenter<V> {
    void loadConditions();
}
