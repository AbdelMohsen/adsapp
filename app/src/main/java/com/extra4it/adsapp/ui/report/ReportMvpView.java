package com.extra4it.adsapp.ui.report;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface ReportMvpView extends ActivityMvpView {
    void clearReportData();
}
