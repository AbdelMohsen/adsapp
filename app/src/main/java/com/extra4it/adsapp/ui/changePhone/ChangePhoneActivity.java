package com.extra4it.adsapp.ui.changePhone;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;
import com.github.ybq.android.spinkit.SpinKitView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePhoneActivity extends BaseActivity implements ChangePhoneMvpView {

    @Inject
    ChangePhoneMvpPresenter<ChangePhoneMvpView> presenter;
    @BindView(R.id.tv_change_phone)
    TextView tvChangePhone;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_old_phone_num)
    TextView tvOldPhoneNum;
    @BindView(R.id.et_old_phone_num)
    EditText etOldPhoneNum;
    @BindView(R.id.tv_new_phone_num)
    TextView tvNewPhoneNum;
    @BindView(R.id.et_new_phone_num)
    EditText etNewPhoneNum;
    @BindView(R.id.tv_enter_password)
    TextView tvEnterPassword;
    @BindView(R.id.tv_password)
    TextView tvPassword;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;
    @BindView(R.id.change_phone_container)
    RelativeLayout changePhoneContainer;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_phoen);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize clickListener
        onItemClicked();
        // initialize Fonts
        setupFonts();
        // change status bar
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
    }

    @Override
    protected void onItemClicked() {
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.changePhoneNum(progressBar, changePhoneContainer, etOldPhoneNum, etNewPhoneNum, etPassword);
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvChangePhone, tvEnterPassword}, this, 2);
        Fonts.setFont(new TextView[]{tvNewPhoneNum, tvOldPhoneNum, tvPassword}, this, 1);
        Fonts.setFont(new Button[]{btnConfirm}, this, 1);
        Fonts.setFont(new EditText[]{etNewPhoneNum, etOldPhoneNum, etPassword}, this, 1);

    }
}
