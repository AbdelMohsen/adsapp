package com.extra4it.adsapp.ui.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {
    void changeFragment(ImageView currentTab, Fragment fragment, String TAG, String toolbarTitle);

    void initializeBeginningPage();

    void initializeResideMenu();

    void openSideMenuContent(Context context, Class<?> cls);

    void initializeUserData();
}
