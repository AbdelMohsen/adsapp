package com.extra4it.adsapp.ui.base;

/**
 * Created by user on 16/05/2018.
 */

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

    void onDetach();

    void setUserAsLoggedOut();
}
