package com.extra4it.adsapp.ui.recharge;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class RechargePresenter<V extends RechargeMvpView> extends BasePresenter<V>
        implements RechargeMvpPresenter<V> {
    @Inject
    public RechargePresenter(DataManager dataManager) {
        super(dataManager);
    }
}
