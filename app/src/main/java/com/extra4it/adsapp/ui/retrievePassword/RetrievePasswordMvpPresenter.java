package com.extra4it.adsapp.ui.retrievePassword;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface RetrievePasswordMvpPresenter<V extends RetrievePasswordMvpView> extends MvpPresenter<V> {
    void onConfirmPhoneClicked();

    void onConfirmCodeClicked();

    void onConfirmPasswordClicked();

}
