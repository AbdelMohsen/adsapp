package com.extra4it.adsapp.ui.confirmPay;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.Fonts;
import com.extra4it.adsapp.utils.PickImage;
import com.extra4it.adsapp.utils.RunTimePermission;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.extra4it.adsapp.utils.AppConstants.CAMERA_REQUEST_CODE;
import static com.extra4it.adsapp.utils.AppConstants.IMAGE_REQUEST_CODE;

public class ConfirmPayActivity extends BaseActivity implements ConfirmPayMvpView, ConfirmDialog.OnConfirmCallback {

    @Inject
    ConfirmPayMvpPresenter<ConfirmPayMvpView> presenter;
    @Inject
    RunTimePermission permission;
    @Inject
    PickImage pickImage;
    // widgets
    @BindView(R.id.tv_confirm_pay)
    TextView tvConfirmPay;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_currency_amount)
    TextView tvCurrencyAmount;
    @BindView(R.id.et_currency_amount)
    EditText etCurrencyAmount;
    @BindView(R.id.tv_currency)
    TextView tvCurrency;
    @BindView(R.id.tv_bank_name)
    TextView tvBankName;
    @BindView(R.id.spinner_bank_name)
    Spinner spinnerBankName;
    @BindView(R.id.tv_check_num)
    TextView tvCheckNum;
    @BindView(R.id.et_check_num)
    EditText etCheckNum;
    @BindView(R.id.tv_check_name)
    TextView tvCheckName;
    @BindView(R.id.et_check_name)
    EditText etCheckName;
    @BindView(R.id.tv_details)
    TextView tvDetails;
    @BindView(R.id.et_details)
    EditText etDetails;
    @BindView(R.id.tv_attach_pic)
    TextView tvAttachPic;
    @BindView(R.id.img_check)
    ImageView imgCheck;
    @BindView(R.id.btn_send)
    Button btnSend;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_pay);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize clickListener
        onItemClicked();
        // initialize Fonts
        setupFonts();
        // change status bar color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));

    }

    @Override
    protected void onItemClicked() {
        imgCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                permission.selectImage();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ConfirmDialog().show(getSupportFragmentManager(), "confirmDialog");
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvDetails, tvAttachPic, tvBankName, tvCheckName, tvCheckNum, tvConfirmPay, tvCurrency, tvCurrencyAmount}, this, 1);
        Fonts.setFont(new EditText[]{etCheckName, etCheckNum, etCurrencyAmount, etDetails}, this, 1);
        Fonts.setFont(new Button[]{btnSend}, this, 1);

    }

    @Override
    public void onConfirmClick() {
        closeActivity();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == IMAGE_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    pickImage.openGallery();
                }
            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {

            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    pickImage.openCamera();
                }

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        pickImage.onActivityResult((BasePresenter) presenter, requestCode, resultCode, data);
    }


    @Override
    public void loadCheckImage(Bitmap bitmap) {
        imgCheck.setImageBitmap(bitmap);
    }
}
