package com.extra4it.adsapp.ui.register.city;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface CityMvpPresenter<V extends CityMvpView> extends MvpPresenter<V> {

    void loadCities();
}
