package com.extra4it.adsapp.ui.user;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class UserPresenter<V extends UserMvpView> extends BasePresenter<V>
        implements UserMvpPresenter<V> {

    @Inject
    public UserPresenter(DataManager dataManager) {
        super(dataManager);
    }
}
