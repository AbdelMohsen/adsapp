package com.extra4it.adsapp.ui.contact;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactActivity extends BaseActivity implements ContactMvpView {

    @Inject
    ContactMvpPresenter<ContactMvpView> presenter;
    @BindView(R.id.tv_contact)
    TextView tvContact;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_want_contact)
    TextView tvWantContact;
    @BindView(R.id.tv_contact_description)
    TextView tvContactDescription;
    @BindView(R.id.tv_have_note)
    TextView tvHaveNote;
    @BindView(R.id.et_message)
    EditText etMessage;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.contact_container)
    LinearLayout contactContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        ButterKnife.bind(this);

        // initialization
        init();
    }

    @Override
    protected void init() {
        // injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize click listener
        onItemClicked();
        // initialize Fonts
        setupFonts();

    }

    @Override
    protected void onItemClicked() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.contact(contactContainer, etMessage);
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvContact, tvHaveNote, tvWantContact}, this, 2);
        Fonts.setFont(new TextView[]{tvContactDescription}, this, 1);
        Fonts.setFont(new EditText[]{etMessage}, this, 1);
        Fonts.setFont(new Button[]{btnSend}, this, 1);

    }

    @Override
    public void clearData(EditText etContact) {
        etContact.setText("");
    }
}
