package com.extra4it.adsapp.ui.rating;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface RatingMvpPresenter<V extends RatingMvpView> extends MvpPresenter<V> {
    void onCloseClicked();

}
