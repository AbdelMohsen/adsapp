package com.extra4it.adsapp.ui.advertisment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.SelectedAdResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.AppConstants;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;

    private CommentCallBack commentCallBack;
    private Context context;
    private List<SelectedAdResponse.Comments> commentList = new ArrayList<>();
    private LayoutInflater inflater;

    public CommentAdapter(Context context, List<SelectedAdResponse.Comments> commentList) {
        this.context = context;
        this.commentList = commentList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_comment, parent, false);
                return new CommentViewHolder(normalView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        if (commentList != null && commentList.size() > 0) {
            return commentList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (commentList != null && commentList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public void setCommentCallBack(CommentCallBack commentCallBack) {
        this.commentCallBack = commentCallBack;
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class CommentViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.img_avatar)
        CircleImageView imgAvatar;
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.tv_comment_date)
        TextView tvCommentDate;
        @BindView(R.id.tv_comment)
        TextView tvComment;

        public CommentViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvComment, tvCommentDate, tvUserName}, context, 1);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            tvComment.setText(commentList.get(position).getComment());
            tvCommentDate.setText(commentList.get(position).getCreated_at());
            Glide.with(context).load(AppConstants.BASE_IMAGE_URL + commentList.get(position).getUsers().get(0).getImg()).into(imgAvatar);
        }

        @Override
        public void onClick(View view) {
            if (commentCallBack != null) {
                commentCallBack.onCommentClicked(itemView, getAdapterPosition());
            }
        }
    }

    public interface CommentCallBack {
        void onCommentClicked(View view, int position);
    }
}
