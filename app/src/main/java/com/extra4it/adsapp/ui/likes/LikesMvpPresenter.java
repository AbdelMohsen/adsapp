package com.extra4it.adsapp.ui.likes;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface LikesMvpPresenter<V extends LikesMvpView> extends MvpPresenter<V> {
}
