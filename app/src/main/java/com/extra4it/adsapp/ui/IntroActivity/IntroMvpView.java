package com.extra4it.adsapp.ui.IntroActivity;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface IntroMvpView extends ActivityMvpView {
    void slideIntro();

    void openLoginActivity();
}
