package com.extra4it.adsapp.ui.likes;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LikesActivity extends BaseActivity implements LikesMvpView, LikesAdapter.LikesClickCallBack {
    @Inject
    LikesPresenter<LikesMvpView> presenter;
    @BindView(R.id.tv_likes)
    TextView tvLikes;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.rv_likes)
    RecyclerView rvLikes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes);
        ButterKnife.bind(this);

        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize click
        onItemClicked();
        // initialize fonts
        setupFonts();
        // change statusBar
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // initialize likes recycler
        rvLikes.setLayoutManager(new LinearLayoutManager(this));
        LikesAdapter likesAdapter = new LikesAdapter(this, null);
        rvLikes.setAdapter(likesAdapter);
    }

    @Override
    protected void onItemClicked() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvLikes}, this, 2);
    }

    @Override
    public void onUserClickListener(View view, int position) {
        // go to user profile
    }
}
