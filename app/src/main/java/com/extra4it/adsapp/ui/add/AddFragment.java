package com.extra4it.adsapp.ui.add;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseFragment;

import javax.inject.Inject;

public class AddFragment extends BaseFragment {

    @Inject
    AddMvpPresenter<AddMvpView> presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add, container, false);
        return view;
    }

    @Override
    protected void onItemClicked() {

    }

    @Override
    protected void setup(View view) {

    }

    @Override
    protected void setupFonts() {

    }

    @Override
    public void openActivity(Context context, Class<?> cls) {

    }
}
