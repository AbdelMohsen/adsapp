package com.extra4it.adsapp.ui.advertisment;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface AdvMvpPresenter<V extends AdvMvpView> extends MvpPresenter<V> {
    void loadAdData(ProgressBar progressBar, int adId);

    void addRating(View view, int adId, int rate);

    void addFav(View view, int adId);

    void addLike(View view, int adId);

    void addComment(View view, int adId, EditText etComment);

}
