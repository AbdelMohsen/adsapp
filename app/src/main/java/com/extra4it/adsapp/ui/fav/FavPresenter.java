package com.extra4it.adsapp.ui.fav;

import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavPresenter<V extends FavMvpView> extends BasePresenter<V>
        implements FavMvpPresenter<V> {

    @Inject
    public FavPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void loadUserFavData(final ProgressBar progressBar) {
        getMvpView().showLoading(progressBar);
        getDataManager().getApiHelper().getUserFavAds("Bearer " + getDataManager().getAccessToken())
                .enqueue(new Callback<BrandResponse>() {
                    @Override
                    public void onResponse(Call<BrandResponse> call, Response<BrandResponse> response) {
                        getMvpView().hideLoading(progressBar);
                        if (response.body().getStatus() == true) {
                            getMvpView().showUserFavData(response);
                        }
                    }

                    @Override
                    public void onFailure(Call<BrandResponse> call, Throwable t) {
                        getMvpView().hideLoading(progressBar);
                    }
                });
    }
}
