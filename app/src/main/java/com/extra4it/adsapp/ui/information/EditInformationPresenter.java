package com.extra4it.adsapp.ui.information;

import android.graphics.Bitmap;
import android.net.Uri;
import android.widget.EditText;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.UpdateResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.PickImage;
import com.extra4it.adsapp.utils.Validation;

import java.io.File;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditInformationPresenter<V extends EditInformationMvpView> extends BasePresenter<V>
        implements EditInformationMvpPresenter<V>, PickImage.ChooseImageCallBack {

    private MultipartBody.Part userProfilePic;

    @Inject
    public EditInformationPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onChangeUnFocuseBackground() {
        getMvpView().releaseBackGround();
    }

    @Override
    public void loadUserData() {
        getMvpView().loadUSerInformation(getDataManager().getCurrentUserProfilePicUrl(),
                getDataManager().getCurrentUserName(), getDataManager().getCurrentUserEmail());
    }

    @Override
    public void updateUserInfo(EditText etUserName, EditText userMail) {
        if (Validation.validate(getMvpView().getContext(), new EditText[]{etUserName, userMail})) {
            getDataManager().getApiHelper().updateUserData("Bearer " + getDataManager().getAccessToken(), RequestBody.create(MediaType.parse("text/plain"), etUserName.getText().toString()),
                    RequestBody.create(MediaType.parse("text/plain"), userMail.getText().toString()), userProfilePic)
                    .enqueue(new Callback<UpdateResponse>() {
                        @Override
                        public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                            if (response.body() != null) {
                                if (response.body().getStatus() != false) {
                                    getDataManager().setCurrentUserId(response.body().getData().getId());
                                    getDataManager().setCurrentUserProfilePicURL(response.body().getData().getImg());
                                    getDataManager().setCurrentUserEmail(response.body().getData().getEmail());
                                    getDataManager().setCurrentUserName(response.body().getData().getName());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<UpdateResponse> call, Throwable t) {

                        }
                    });
        }
    }

    @Override
    public void onChooseImage(Uri uri, String realPath, File file, Bitmap bitmap) {
        getMvpView().loadChooseImage(bitmap);
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        userProfilePic = MultipartBody.Part.createFormData("img", file.getName(), mFile);

    }
}
