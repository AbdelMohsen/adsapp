package com.extra4it.adsapp.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.ProgressBar;

import com.extra4it.adsapp.di.component.ActivityComponent;

public abstract class BaseBottomSheet extends BottomSheetDialogFragment implements MvpView {
    private BaseActivity baseActivity;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setup(view);
        setupFonts();
        onItemClicked();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) context;
            this.baseActivity = baseActivity;
            baseActivity.onFragmentAttached();
        }
    }

    public ActivityComponent getActivityComponent() {
        if (baseActivity != null) {
            return baseActivity.getActivityComponent();
        }
        return null;
    }

    protected abstract void onItemClicked();

    protected abstract void setup(View view);

    protected abstract void setupFonts();

    @Override
    public void showLoading(ProgressBar progressBar) {

    }

    @Override
    public void hideLoading(ProgressBar progressBar) {

    }

    @Override
    public void showMessage(View view, String message) {

    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public boolean inNetworkConnected() {
        return false;
    }

    @Override
    public void showSnackBar(View view, String message) {

    }

    @Override
    public void openActivity(Context context, Class<?> cls) {

    }
}
