package com.extra4it.adsapp.ui.register.city;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityPresenter<V extends CityMvpView> extends BasePresenter<V>
        implements CityMvpPresenter<V> {

    @Inject
    public CityPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void loadCities() {
        getDataManager().getApiHelper().getCities().enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                if (response.body().getStatus() == true) {
                    getMvpView().showCities(response);

                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });
    }
}
