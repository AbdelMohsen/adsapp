package com.extra4it.adsapp.ui.home;

import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface HomeMvpPresenter<V extends HomeMvpView> extends MvpPresenter<V> {
    void loadHomeTags();

    void loadSubSections(ProgressBar progressBar, int tagId);
}
