package com.extra4it.adsapp.ui.recharge;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.confirmPay.ConfirmPayActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RechargeActivity extends BaseActivity implements RechargeMvpView {

    @Inject
    RechargeMvpPresenter<RechargeMvpView> presenter;
    @BindView(R.id.tv_recharge_title)
    TextView tvRechargeTitle;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_recharge)
    TextView tvRecharge;
    @BindView(R.id.tv_account_status)
    TextView tvAccountStatus;
    @BindView(R.id.tv_recharge_description)
    TextView tvRechargeDescription;
    @BindView(R.id.btn_confirm_pay)
    Button btnConfirmPay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize click listener
        onItemClicked();
        // initialize fonts
        setupFonts();
        // change status bar
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
    }

    @Override
    protected void onItemClicked() {
        btnConfirmPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(RechargeActivity.this, ConfirmPayActivity.class);
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvAccountStatus, tvRechargeDescription, tvRecharge}, this, 1);
        Fonts.setFont(new TextView[]{tvRechargeTitle}, this, 2);
        Fonts.setFont(new Button[]{btnConfirmPay}, this, 1);
    }
}
