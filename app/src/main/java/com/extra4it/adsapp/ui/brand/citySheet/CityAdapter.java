package com.extra4it.adsapp.ui.brand.citySheet;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class CityAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private Context context;
    private List<CityResponse.Data> cityList = new ArrayList<>();
    private LayoutInflater inflater;
    private CityCallBack cityCallBack;
    private int rowIndex = -1;

    public CityAdapter(Context context, List<CityResponse.Data> cityList) {
        this.context = context;
        this.cityList = cityList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_city, parent, false);
                return new CityViewHolder(normalView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        if (cityList != null && cityList.size() > 0) {
            return cityList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (cityList != null && cityList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public void setCityCallBack(CityCallBack cityCallBack) {
        this.cityCallBack = cityCallBack;
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class CityViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_city_name)
        TextView tvCityName;

        public CityViewHolder(View itemView) {
            super(itemView);
            tvCityName.setOnClickListener(this);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvCityName}, context, 1);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            tvCityName.setText(cityList.get(position).getCity_name());
            if (rowIndex == position) {
                tvCityName.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                tvCityName.setTextColor(context.getResources().getColor(R.color.colorWhite));
            } else {
                tvCityName.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                tvCityName.setTextColor(context.getResources().getColor(R.color.colorBlack));
            }
        }

        @Override
        public void onClick(View view) {
            rowIndex = getAdapterPosition();
            notifyDataSetChanged();
            if (cityCallBack != null) {
                cityCallBack.onCityClick(rowIndex);
            }
        }
    }

    public interface CityCallBack {
        void onCityClick(int position);
    }

}
