package com.extra4it.adsapp.ui.login;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {
    void onNewRegisterClicked();

    void onRetrievePasswordClicked();

    void onLoginButtonClicked(ProgressBar progressBar, View view, EditText etUserName, EditText etPassword);



}
