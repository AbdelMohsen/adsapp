package com.extra4it.adsapp.ui.confirmPay;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.PickImage;

import java.io.File;

import javax.inject.Inject;

public class ConfirmPayPresenter<V extends ConfirmPayMvpView> extends BasePresenter<V>
        implements ConfirmPayMvpPresenter<V>, PickImage.ChooseImageCallBack {

    @Inject
    public ConfirmPayPresenter(DataManager dataManager) {
        super(dataManager);
    }

    Uri uri;
    String realPath;
    Bitmap bitmap;
    @Override
    public void onChooseImage(Uri uri, String realPath, File file, Bitmap bitmap) {
        Log.e("xxx", "Confirm pay presenter : "+realPath);
        this.uri = uri;
        this.realPath = realPath;
        this.bitmap = bitmap;
        getMvpView().loadCheckImage(bitmap);
    }
}
