package com.extra4it.adsapp.ui.user;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class UserAdsAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;


    private Context context;
    private List<BrandResponse> brandList = new ArrayList<>();
    private LayoutInflater inflater;

    public UserAdsAdapter(Context context, List<BrandResponse> brandList) {
        this.context = context;
        this.brandList = brandList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_brand, parent, false);
                return new BrandViewHolder(normalView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return 10;
//        return brandList.size();
    }

    @Override
    public int getItemViewType(int position) {
//        if (brandList != null && brandList.size() > 0) {
//            return VIEW_TYPE_NORMAL;
//        } else {
//            return VIEW_TYPE_EMPTY;
//        }
        return VIEW_TYPE_NORMAL;
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class BrandViewHolder extends BaseViewHolder {
        @BindView(R.id.img_brand)
        ImageView imgBrand;
        @BindView(R.id.tv_ad_description)
        TextView tvAdDescription;
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.tv_date)
        TextView tvDate;

        public BrandViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvAdDescription, tvDate, tvUserName}, context, 1);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
        }
    }
}
