package com.extra4it.adsapp.ui.register.city;

import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.ui.base.DialogMvpView;

import retrofit2.Response;

public interface CityMvpView extends DialogMvpView {
    void showCities(Response<CityResponse> response);
}
