package com.extra4it.adsapp.ui.base;





/* NOTES
 * Permission
 * */


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.extra4it.adsapp.AdsApp;
import com.extra4it.adsapp.R;
import com.extra4it.adsapp.di.component.ActivityComponent;
import com.extra4it.adsapp.di.component.DaggerActivityComponent;
import com.extra4it.adsapp.di.module.ActivityModule;
import com.extra4it.adsapp.utils.CommonUtils;

/**
 * Created by user on 16/05/2018.
 */

public abstract class BaseActivity extends AppCompatActivity
        implements ActivityMvpView, BaseFragment.CallBack {
    private static final String TAG = "BaseActivity";
    private String currentFragmentTag = null;
    // Activity Component
    ActivityComponent activityComponent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((AdsApp) getApplication()).getApplicationComponent())
                .build();

    }


    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    @Override
    public void showLoading(ProgressBar progressBar) {
        CommonUtils.showProgressBar(progressBar, this);
    }

    @Override
    public void hideLoading(ProgressBar progressBar) {
        CommonUtils.hideProgressBar(progressBar, this);
    }

    @Override
    public void showMessage(View view, String message) {
        showSnackBar(view, message);
    }

    @Override
    public void hideKeyboard() {
        CommonUtils.hideKeyboard(this);
    }

    @Override
    public boolean inNetworkConnected() {
        return false;
    }

    @Override
    public void showSnackBar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView sbTextView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            sbTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            sbTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        sbTextView.setTextColor(getResources().getColor(R.color.colorAccent));
        sbTextView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    protected abstract void init();

    protected abstract void onItemClicked();

    protected abstract void setupFonts();

    @Override
    public void onFragmentAttached() {
        Log.e(TAG, "onFragmentAttached: ");
    }

    @Override
    public void onFragmentDetached(String tag) {
        Log.e(TAG, "onFragmentDetached: " + tag);
    }

    @Override
    public void closeActivity() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.scale_out);
    }

    @Override
    public void loadCurrentFragment(Fragment fragment, String TAG) {
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        this.currentFragmentTag = TAG;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, TAG)
                .commit();
    }

    @Override
    public Context getContext() {
        return BaseActivity.this;
    }

    @Override
    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    public String getCurrentFragmentTag() {
        return currentFragmentTag;
    }

    @Override
    public void openActivity(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        startActivity(intent);
        overridePendingTransition(R.anim.push_up_out, R.anim.fade_out);
    }

    @Override
    public void hideStatusBar() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }


}
