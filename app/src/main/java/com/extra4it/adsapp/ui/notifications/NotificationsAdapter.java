package com.extra4it.adsapp.ui.notifications;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.NotificationsResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class NotificationsAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_Comment = 1;
    private static final int VIEW_TYPE_Like = 2;


    private Context context;
    private List<NotificationsResponse> notificationsList = new ArrayList<>();
    private LayoutInflater inflater;

    @Inject
    public NotificationsAdapter(Context context, List<NotificationsResponse> notificationsList) {
        this.context = context;
        this.notificationsList = notificationsList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_Comment:
                View commentView = inflater.inflate(R.layout.row_comment, parent, false);
                return new CommentViewHolder(commentView);
            case VIEW_TYPE_Like:
                View likeView = inflater.inflate(R.layout.row_notification_like, parent, false);
                return new LikeViewHolder(likeView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return 5;
//        return notificationsList.size();
    }

    @Override
    public int getItemViewType(int position) {
//        if (notificationsList != null && notificationsList.size() > 0) {
//            if (notificationsList.get(position).getLike){
//                return VIEW_TYPE_Like;
//            }else {
//                return VIEW_TYPE_Like;
//            }
//        } else {
//            return VIEW_TYPE_EMPTY;
//        }
        return VIEW_TYPE_Like;

    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class CommentViewHolder extends BaseViewHolder {

        public CommentViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
        }
    }

    class LikeViewHolder extends BaseViewHolder {
        public LikeViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
        }
    }
}
