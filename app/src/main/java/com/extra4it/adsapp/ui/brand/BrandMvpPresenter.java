package com.extra4it.adsapp.ui.brand;

import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface BrandMvpPresenter<V extends BrandMvpView> extends MvpPresenter<V> {

    void loadBrandAds(ProgressBar progressBar, int subSectionId);

    void loadBrandAds(ProgressBar progressBar, int subSectionId, int cityId);
}
