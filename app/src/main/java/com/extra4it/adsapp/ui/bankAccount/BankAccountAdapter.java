package com.extra4it.adsapp.ui.bankAccount;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.BankAccountResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BankAccountAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;


    private Context context;
    private List<BankAccountResponse.Banks> bankAccountList = new ArrayList<>();
    private LayoutInflater inflater;

    public BankAccountAdapter(Context context, List<BankAccountResponse.Banks> bankAccountList) {
        this.context = context;
        this.bankAccountList = bankAccountList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_bank_account, parent, false);
                return new BankAccountViewHolder(normalView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {

        if (bankAccountList != null && bankAccountList.size() > 0) {
            return bankAccountList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (bankAccountList != null && bankAccountList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class BankAccountViewHolder extends BaseViewHolder {
        @BindView(R.id.tv_bank_name)
        TextView tvBankName;
        @BindView(R.id.tv_account_name)
        TextView tvAccountName;
        @BindView(R.id.tv_account_name_status)
        TextView tvAccountNameStatus;
        @BindView(R.id.tv_account_num)
        TextView tvAccountNum;
        @BindView(R.id.tv_account_num_status)
        TextView tvAccountNumStatus;

        public BankAccountViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvAccountName, tvAccountNameStatus, tvAccountNum, tvAccountNumStatus}, context, 1);
            Fonts.setFont(new TextView[]{tvBankName}, context, 2);

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            tvBankName.setText(bankAccountList.get(position).getBank_name());
            tvAccountNumStatus.setText(bankAccountList.get(position).getAcc_num());
            tvAccountNameStatus.setText(bankAccountList.get(position).getAcc_name());
        }
    }
}
