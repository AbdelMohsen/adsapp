package com.extra4it.adsapp.ui.base;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.di.component.ActivityComponent;
import com.extra4it.adsapp.utils.CommonUtils;

/**
 * Created by user on 22/05/2018.
 */

public abstract class BaseFragment extends Fragment implements MvpView {

    private BaseActivity baseActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) context;
            this.baseActivity = baseActivity;
            baseActivity.onFragmentAttached();
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setup(view);
        setupFonts();
        onItemClicked();
    }

    protected abstract void onItemClicked();

    protected abstract void setup(View view);

    protected abstract void setupFonts();

    @Override
    public void showLoading(ProgressBar progressBar) {
        CommonUtils.showProgressBar(progressBar, this.getActivity());
    }

    @Override
    public void hideLoading(ProgressBar progressBar) {
        CommonUtils.hideProgressBar(progressBar, this.getActivity());

    }

    @Override
    public void showMessage(View view, String message) {
        if (baseActivity != null) {
            baseActivity.showMessage(view, message);
        }
    }

    @Override
    public void hideKeyboard() {
        CommonUtils.hideKeyboard(this.getActivity());
    }

    @Override
    public boolean inNetworkConnected() {
        return false;
    }

    @Override
    public void showSnackBar(View view, String message) {
        if (baseActivity != null) {
            baseActivity.showSnackBar(view, message);
        }
    }

    public ActivityComponent getActivityComponent() {
        if (baseActivity != null) {
            return baseActivity.getActivityComponent();
        }
        return null;
    }

    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    public interface CallBack {
        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }

    @Override
    public void openActivity(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        startActivity(intent);
        getBaseActivity().overridePendingTransition(R.anim.push_up_out, R.anim.fade_out);
    }
}
