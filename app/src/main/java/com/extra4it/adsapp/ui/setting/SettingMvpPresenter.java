package com.extra4it.adsapp.ui.setting;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface SettingMvpPresenter<V extends SettingMvpView> extends MvpPresenter<V> {
}
