package com.extra4it.adsapp.ui.report;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;
import com.github.ybq.android.spinkit.SpinKitView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportActivity extends BaseActivity implements ReportMvpView {
    // injection
    @Inject
    ReportMvpPresenter<ReportMvpView> presenter;
    //widgets
    @BindView(R.id.tv_report)
    TextView tvReport;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_report_about_violent)
    TextView tvReportAboutViolent;
    @BindView(R.id.tv_report_about_violent_description)
    TextView tvReportAboutViolentDescription;
    @BindView(R.id.tv_reason_of_violent)
    TextView tvReasonOfViolent;
    @BindView(R.id.et_reason_of_violent)
    EditText etReasonOfViolent;
    @BindView(R.id.btn_report)
    Button btnReport;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;
    @BindView(R.id.report_container)
    RelativeLayout reportContainer;
    private int adId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize clickListener
        onItemClicked();
        // initialize fonts
        setupFonts();
        // change status color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // getData
        if (getIntent() != null) {
            adId = getIntent().getIntExtra("adId", 0);
        }
    }

    @Override
    protected void onItemClicked() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });
        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.addReport(progressBar, reportContainer, etReasonOfViolent, adId);
            }
        });

    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvReasonOfViolent, tvReportAboutViolent, tvReport}, this, 2);
        Fonts.setFont(new TextView[]{tvReportAboutViolentDescription}, this, 1);
        Fonts.setFont(new EditText[]{etReasonOfViolent}, this, 1);
        Fonts.setFont(new Button[]{btnReport}, this, 1);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    @Override
    public void clearReportData() {
        etReasonOfViolent.setText("");
    }
}
