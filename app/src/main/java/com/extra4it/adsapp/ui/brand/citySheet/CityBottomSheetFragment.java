package com.extra4it.adsapp.ui.brand.citySheet;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.ui.base.BaseBottomSheet;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

public class CityBottomSheetFragment extends BaseBottomSheet implements CitySheetMvpView, CityAdapter.CityCallBack {
    @Inject
    CitySheetPresenter<CitySheetMvpView> presenter;
    @BindView(R.id.rv_city)
    RecyclerView rvCity;
    Unbinder unbinder;
    @BindView(R.id.bottom_sheet)
    ConstraintLayout bottomSheet;

    CityBottomSheetCallBack cityBottomSheetCallBack;
    private List<CityResponse.Data> cityList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_bottom_sheet, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onItemClicked() {

    }

    @Override
    protected void setup(View view) {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);


        // getCities
        presenter.loadCities();

    }

    @Override
    protected void setupFonts() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCityClick(int position) {
        Log.e("xxx", "" + position);
        dismiss();
        if (cityBottomSheetCallBack != null) {
            cityBottomSheetCallBack.onCityBottomSheetClick(position, cityList.get(position).getId());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CityBottomSheetCallBack) {
            cityBottomSheetCallBack = (CityBottomSheetCallBack) context;
        }
    }

    @Override
    public void showCities(Response<CityResponse> response) {
        cityList = response.body().getData();
        // initialize city recyclerView
        if (getActivity() != null) {
            rvCity.setLayoutManager(new LinearLayoutManager(getActivity()));
            CityAdapter cityAdapter = new CityAdapter(getActivity(), cityList);
            cityAdapter.setCityCallBack(this);
            rvCity.setAdapter(cityAdapter);
        }
    }

    public interface CityBottomSheetCallBack {
        void onCityBottomSheetClick(int position, int cityId);
    }
}
