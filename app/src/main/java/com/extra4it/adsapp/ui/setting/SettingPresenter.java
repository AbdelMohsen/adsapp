package com.extra4it.adsapp.ui.setting;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class SettingPresenter<V extends SettingMvpView> extends BasePresenter<V>
    implements SettingMvpPresenter<V>{

    @Inject
    public SettingPresenter(DataManager dataManager) {
        super(dataManager);
    }
}
