package com.extra4it.adsapp.ui.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V>
        implements MainMvpPresenter<V> {

    @Inject
    public MainPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void changeFragment(ImageView currentTab, Fragment fragment, String TAG, String toolbarTitle) {
        getMvpView().releaseSelection(currentTab);
        getMvpView().loadCurrentFragment(fragment, TAG);
        getMvpView().updateToolbar(toolbarTitle);
        getMvpView().hideSideMenu();
    }

    @Override
    public void initializeBeginningPage() {
        getMvpView().initializeBeginningPage();
    }

    @Override
    public void initializeResideMenu() {
        getMvpView().initializeResideMenuViews();
    }

    @Override
    public void openSideMenuContent(Context context, Class<?> cls) {
        getMvpView().openActivity(context, cls);
        getMvpView().hideSideMenu();
    }

    @Override
    public void initializeUserData() {
        getMvpView().loadUserData(getDataManager().getCurrentUserProfilePicUrl(),
                getDataManager().getCurrentUserName(),
                getDataManager().getCurrentUserPhoneNum(),
                getDataManager().getCurrentUserEmail());
    }
}
