package com.extra4it.adsapp.ui.conditions;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface ConditionsMvpView extends ActivityMvpView {
    void showConditions(String terms);
}
