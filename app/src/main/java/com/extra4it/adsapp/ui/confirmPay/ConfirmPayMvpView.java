package com.extra4it.adsapp.ui.confirmPay;

import android.graphics.Bitmap;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface ConfirmPayMvpView extends ActivityMvpView {

    void loadCheckImage(Bitmap bitmap);

}
