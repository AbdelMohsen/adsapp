package com.extra4it.adsapp.ui.login;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface LoginMvpView extends ActivityMvpView {

    void openRegisterActivity();

    void openRetrievePasswordActivity();

    void openMainActivity();
}
