package com.extra4it.adsapp.ui.searchResult;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface SearchResultMvpPresenter<V extends SearchResultMvpView> extends MvpPresenter<V> {
}
