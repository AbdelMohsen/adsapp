package com.extra4it.adsapp.ui.brand.citySheet;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CitySheetPresenter<V extends CitySheetMvpView> extends BasePresenter<V>
        implements CitySheetMvpPresenter<V> {

    @Inject
    public CitySheetPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void loadCities() {
        getDataManager().getApiHelper().getCities()
                .enqueue(new Callback<CityResponse>() {
                    @Override
                    public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                        getMvpView().showCities(response);

                    }

                    @Override
                    public void onFailure(Call<CityResponse> call, Throwable t) {

                    }
                });
    }
}
