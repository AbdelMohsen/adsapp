package com.extra4it.adsapp.ui.changePhone;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.DefaultResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.Validation;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePhonePresenter<V extends ChangePhoneMvpView> extends BasePresenter<V>
        implements ChangePhoneMvpPresenter<V> {


    @Inject
    public ChangePhonePresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void changePhoneNum(final ProgressBar progressBar, final View view, EditText etOldPhone, final EditText etNewPhone, EditText etPassword) {
        if (Validation.validate(getMvpView().getContext(), new EditText[]{etNewPhone, etOldPhone, etPassword})) {
            getMvpView().showLoading(progressBar);
            getDataManager().getApiHelper().changePhoneNum("Bearer " + getDataManager().getAccessToken(), etOldPhone.getText().toString(),
                    etPassword.getText().toString(), etNewPhone.getText().toString())
                    .enqueue(new Callback<DefaultResponse>() {
                        @Override
                        public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                            if (response.body() != null) {
                                if (response.body().getStatus() == true) {
                                    getMvpView().showMessage(view, response.body().getMsg());
                                    getDataManager().setCurrentUserPhoneNum(etNewPhone.getText().toString());
                                }
                            }
                            getMvpView().hideLoading(progressBar);
                        }

                        @Override
                        public void onFailure(Call<DefaultResponse> call, Throwable t) {
                            getMvpView().hideLoading(progressBar);

                        }
                    });
        }
    }
}
