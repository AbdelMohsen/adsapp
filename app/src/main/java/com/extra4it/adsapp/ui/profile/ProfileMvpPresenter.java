package com.extra4it.adsapp.ui.profile;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface ProfileMvpPresenter<V extends ProfileMvpView> extends MvpPresenter<V> {
    void getUserData();
}
