package com.extra4it.adsapp.ui.user;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserActivity extends BaseActivity implements UserMvpView {
    // injection
    @Inject
    UserMvpPresenter<UserMvpView> presenter;
    @Inject
    UserAdsAdapter userAdsAdapter;
    @Inject
    LinearLayoutManager llManager;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_avatar)
    ImageView imgAvatar;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.tv_user_ads)
    TextView tvUserAds;
    @BindView(R.id.rv_user_ads)
    RecyclerView rvUserAds;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize click listener
        onItemClicked();
        // initialize fonts
        setupFonts();
        // change statusBar
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // initialize recycler view
        rvUserAds.setLayoutManager(llManager);
        rvUserAds.setAdapter(userAdsAdapter);
        rvUserAds.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(this, R.anim.item_anim));


    }

    @Override
    protected void onItemClicked() {

    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvUserAds, tvUserName}, this, 2);
        Fonts.setFont(new TextView[]{tvCity}, this, 1);

    }
}
