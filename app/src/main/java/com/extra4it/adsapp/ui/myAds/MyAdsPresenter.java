package com.extra4it.adsapp.ui.myAds;

import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyAdsPresenter<V extends MyAdsMvpView> extends BasePresenter<V>
        implements MyAdsMvpPresenter<V> {

    @Inject
    public MyAdsPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void getUserAds(final ProgressBar progressBar) {
        getMvpView().showLoading(progressBar);
        getDataManager().getApiHelper().getUserAds("Bearer " + getDataManager().getAccessToken())
                .enqueue(new Callback<BrandResponse>() {
                    @Override
                    public void onResponse(Call<BrandResponse> call, Response<BrandResponse> response) {
                        if (response.body() != null) {
                            getMvpView().loadUserAds(response);
                        }
                        getMvpView().hideLoading(progressBar);
                    }

                    @Override
                    public void onFailure(Call<BrandResponse> call, Throwable t) {
                        getMvpView().hideLoading(progressBar);
                    }
                });
    }
}
