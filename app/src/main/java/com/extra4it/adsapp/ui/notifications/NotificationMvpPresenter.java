package com.extra4it.adsapp.ui.notifications;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface NotificationMvpPresenter<V extends NotificationMvpView> extends MvpPresenter<V> {
}
