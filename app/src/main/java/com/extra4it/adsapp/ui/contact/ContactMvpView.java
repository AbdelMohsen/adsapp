package com.extra4it.adsapp.ui.contact;

import android.widget.EditText;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface ContactMvpView extends ActivityMvpView {
    void clearData(EditText etContact);
}
