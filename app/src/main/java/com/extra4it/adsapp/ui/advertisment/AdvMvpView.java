package com.extra4it.adsapp.ui.advertisment;

import com.extra4it.adsapp.data.network.model.SelectedAdResponse;
import com.extra4it.adsapp.ui.base.ActivityMvpView;

import retrofit2.Response;

public interface AdvMvpView extends ActivityMvpView {

    void showAdsData(Response<SelectedAdResponse> response);

    void updateView(int status);

    void clearComment();
}
