package com.extra4it.adsapp.ui.register;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.RegisterResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.PickImage;
import com.extra4it.adsapp.utils.Validation;

import java.io.File;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenter<V extends RegisterMvpView> extends BasePresenter<V>
        implements RegisterMvpPresenter<V>, PickImage.ChooseImageCallBack {

    private Uri uri;
    private String realPath;
    private Bitmap bitmap;
    private MultipartBody.Part userProfile;

    @Inject
    public RegisterPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onConfirmClicked(final ProgressBar progressBar, final View view, EditText etUserName, EditText etPhone,
                                 EditText etMail, EditText etPassword, EditText etConfirmPassword, EditText etCity, int cityId) {
        getMvpView().showLoading(progressBar);
        if (Validation.internetConnectionAvailable()) {
            if (Validation.validate(getMvpView().getContext(), new EditText[]{etCity, etMail, etPhone, etUserName, etPassword, etConfirmPassword})) {
                if (Validation.isEmailValid(getMvpView().getContext(), etMail)) {
                    if (Validation.isPasswordValid(getMvpView().getContext(), etPassword)) {
                        if (Validation.isPasswordMatch(getMvpView().getContext(), etPassword, etConfirmPassword)) {
                            getDataManager().getApiHelper().register(RequestBody.create(MediaType.parse("text/plain"), etUserName.getText().toString()),
                                    RequestBody.create(MediaType.parse("text/plain"), etMail.getText().toString()),
                                    RequestBody.create(MediaType.parse("text/plain"), etPassword.getText().toString()),
                                    RequestBody.create(MediaType.parse("text/plain"), etPhone.getText().toString()),
                                    cityId, userProfile).enqueue(new Callback<RegisterResponse>() {
                                @Override
                                public void onResponse(@NonNull Call<RegisterResponse> call, @NonNull Response<RegisterResponse> response) {
                                    getMvpView().hideLoading(progressBar);
                                    if (response.body() != null) {
                                        if (response.body().getStatus() == true) {
                                            new android.os.Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    getMvpView().openLoginActivity();
                                                }
                                            }, 600);
                                        }
                                        getMvpView().showSnackBar(view, response.body().getMsg());

                                    }
                                }

                                @Override
                                public void onFailure(@NonNull Call<RegisterResponse> call, @NonNull Throwable t) {
                                    getMvpView().hideLoading(progressBar);

                                }
                            });
                        }
                    }
                }
            }
        } else {
            getMvpView().showSnackBar(view, "من فضلك تحقق من الانترنت ");
        }
        getMvpView().hideLoading(progressBar);

    }

    @Override
    public void onChooseImage(Uri uri, String realPath, File file, Bitmap bitmap) {
        Log.e("xxx", "Confirm pay presenter : " + realPath);
        this.uri = uri;
        this.realPath = realPath;
        this.bitmap = bitmap;
        getMvpView().loadUserAvatar(bitmap);
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        userProfile = MultipartBody.Part.createFormData("img", file.getName(), mFile);

    }
}
