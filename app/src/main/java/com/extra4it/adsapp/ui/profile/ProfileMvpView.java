package com.extra4it.adsapp.ui.profile;

import com.extra4it.adsapp.data.network.model.UserProfileResponse;
import com.extra4it.adsapp.ui.base.MvpView;

import retrofit2.Response;

public interface ProfileMvpView extends MvpView {
    void loadUserData(Response<UserProfileResponse> response);
}
