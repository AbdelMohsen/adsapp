package com.extra4it.adsapp.ui.register;

import android.graphics.Bitmap;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface RegisterMvpView extends ActivityMvpView {
    void loadUserAvatar(Bitmap bitmap);
    void openLoginActivity();
}
