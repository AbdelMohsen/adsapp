package com.extra4it.adsapp.ui.splash;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.IntroActivity.IntroActivity;
import com.extra4it.adsapp.ui.base.BaseActivity;

import javax.inject.Inject;

public class SplashActivity extends BaseActivity implements SplashMvpView {
    @Inject
    SplashMvpPresenter<SplashMvpView> presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // initialization
        init();

    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize item click listener
        onItemClicked();
        // initialize Fonts
        setupFonts();
        // change statusBar color
        changeStatusBarColor(Color.TRANSPARENT);
        // open decided activity
        presenter.onOpenDecideActivity();

    }

    @Override
    protected void onItemClicked() {

    }

    @Override
    protected void setupFonts() {

    }

    @Override
    public void openIntroActivity() {
        Intent intent = new Intent(this, IntroActivity.class);
        startActivity(intent);
    }

    @Override
    public void openLoginActivity() {

    }

    @Override
    public void openMainActivity() {

    }
}
