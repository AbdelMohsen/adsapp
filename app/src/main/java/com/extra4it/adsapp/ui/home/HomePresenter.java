package com.extra4it.adsapp.ui.home;

import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.HomeTagsResponse;
import com.extra4it.adsapp.data.network.model.SectionResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter<V extends HomeMvpView> extends BasePresenter<V>
        implements HomeMvpPresenter<V> {

    @Inject
    public HomePresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void loadHomeTags() {
        getDataManager().getApiHelper().getTags()
                .enqueue(new Callback<HomeTagsResponse>() {
                    @Override
                    public void onResponse(Call<HomeTagsResponse> call, Response<HomeTagsResponse> response) {
                        if (response.body() != null) {
                            getMvpView().showHomeTags(response);
                        }
                    }

                    @Override
                    public void onFailure(Call<HomeTagsResponse> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadSubSections(final ProgressBar progressBar, int tagId) {
        getMvpView().showLoading(progressBar);
        getDataManager().getApiHelper().getSubSections(tagId)
                .enqueue(new Callback<SectionResponse>() {
                    @Override
                    public void onResponse(Call<SectionResponse> call, Response<SectionResponse> response) {
                        if (response.body() != null) {
                            getMvpView().showSubSections(response);
                            getMvpView().hideLoading(progressBar);
                        }
                    }

                    @Override
                    public void onFailure(Call<SectionResponse> call, Throwable t) {
                        getMvpView().hideLoading(progressBar);
                    }
                });
    }
}
