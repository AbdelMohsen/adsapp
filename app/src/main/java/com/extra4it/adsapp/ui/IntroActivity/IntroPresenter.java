package com.extra4it.adsapp.ui.IntroActivity;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class IntroPresenter<V extends IntroMvpView> extends BasePresenter<V>
        implements IntroMvpPresenter<V> {

    @Inject
    public IntroPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onNextClicked() {
        getMvpView().slideIntro();
    }

    @Override
    public void onSkipClicked() {
        getMvpView().openLoginActivity();
    }
}
