package com.extra4it.adsapp.ui.myAds;

import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.base.ActivityMvpView;

import retrofit2.Response;

public interface MyAdsMvpView extends ActivityMvpView {
    void loadUserAds(Response<BrandResponse> response);
}
