package com.extra4it.adsapp.ui.bankAccount;

import com.extra4it.adsapp.data.network.model.BankAccountResponse;
import com.extra4it.adsapp.ui.base.ActivityMvpView;

import retrofit2.Response;

public interface BankAccountMvpView extends ActivityMvpView {

    void loadBanksData(Response<BankAccountResponse> response);
}
