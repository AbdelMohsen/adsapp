package com.extra4it.adsapp.ui.brand;

import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.base.ActivityMvpView;

import retrofit2.Response;

public interface BrandMvpView extends ActivityMvpView {

    void showBrandAds(Response<BrandResponse> response);


}
