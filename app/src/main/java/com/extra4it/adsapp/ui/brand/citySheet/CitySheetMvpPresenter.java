package com.extra4it.adsapp.ui.brand.citySheet;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface CitySheetMvpPresenter<V extends CitySheetMvpView> extends MvpPresenter<V> {
    void loadCities();
}
