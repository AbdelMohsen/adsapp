package com.extra4it.adsapp.ui.searchResult;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchResultActivity extends BaseActivity implements SearchResultMvpView {

    // injection
    @Inject
    SearchResultMvpPresenter<SearchResultMvpView> presenter;
    @BindView(R.id.tv_search_result)
    TextView tvSearchResult;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.rv_search_result)
    RecyclerView rvSearchResult;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        ButterKnife.bind(this);
        // initialization
        init();
        // initialize clickListener
        onItemClicked();
        // initialize fonts
        setupFonts();

    }

    @Override
    protected void init() {
        // injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // change status color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // initialize recycler view
        rvSearchResult.setLayoutManager(new LinearLayoutManager(this));
        SearchResultAdapter searchResultAdapter = new SearchResultAdapter(this, null);
        rvSearchResult.setAdapter(searchResultAdapter);
    }

    @Override
    protected void onItemClicked() {

    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvSearchResult}, this, 2);
    }
}
