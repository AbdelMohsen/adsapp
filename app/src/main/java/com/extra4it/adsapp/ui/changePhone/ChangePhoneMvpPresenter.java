package com.extra4it.adsapp.ui.changePhone;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface ChangePhoneMvpPresenter<V extends ChangePhoneMvpView> extends MvpPresenter<V> {

    void changePhoneNum(ProgressBar progressBar, View view, EditText etOldPhone, EditText etNewPhone, EditText etPassword);
}
