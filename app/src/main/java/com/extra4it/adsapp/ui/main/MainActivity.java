package com.extra4it.adsapp.ui.main;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.add.AddFragment;
import com.extra4it.adsapp.ui.bankAccount.BankAccountActivity;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.contact.ContactActivity;
import com.extra4it.adsapp.ui.fav.FavFragment;
import com.extra4it.adsapp.ui.home.HomeFragment;
import com.extra4it.adsapp.ui.myAds.MyAdsActivity;
import com.extra4it.adsapp.ui.notifications.NotificationActivity;
import com.extra4it.adsapp.ui.profile.ProfileFragment;
import com.extra4it.adsapp.ui.recharge.RechargeActivity;
import com.extra4it.adsapp.ui.search.SearchFragment;
import com.extra4it.adsapp.ui.setting.SettingActivity;
import com.extra4it.adsapp.utils.Fonts;
import com.yarolegovich.slidingrootnav.SlideGravity;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;
import com.yarolegovich.slidingrootnav.SlidingRootNavLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.extra4it.adsapp.utils.AppConstants.BASE_IMAGE_URL;

public class MainActivity extends BaseActivity implements MainMvpView, View.OnClickListener {
    // injection
    @Inject
    MainMvpPresenter<MainMvpView> presenter;
    // widgets
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_burger)
    ImageView btnBurger;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;

    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;
    @BindView(R.id.img_profile)
    ImageView imgProfile;
    @BindView(R.id.img_fav)
    ImageView imgFav;
    @BindView(R.id.img_add)
    ImageView imgAdd;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.img_home)
    ImageView imgHome;
    @BindView(R.id.tabs_layout)
    LinearLayout tabsLayout;
    @BindView(R.id.img_notification)
    ImageView imgNotification;

    private SlidingRootNav slideMenu;
    private SlidingRootNavLayout slideMenuLayout;

    // Vars
    boolean isOpened = false;
    private static final String TAG_HOME = "FRAGMENT_HOME";
    private static final String TAG_SEARCH = "FRAGMENT_SEARCH";
    private static final String TAG_ADD = "FRAGMENT_ADD";
    private static final String TAG_FAV = "FRAGMENT_FAV";
    private static final String TAG_PROFILE = "FRAGMENT_PROFILE";
    private TextView tvUserName, tvUserPhone, tvUserMail, tvMain,
            tvSetting, tvMyAds, tvParticipation, tv_bank_account, tvContact, tvLogOut;
    private CircleImageView imgAvatar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize resideMenu
        slideMenu = new SlidingRootNavBuilder(this)
                .withGravity(SlideGravity.RIGHT)
                .withMenuLayout(R.layout.layout_reside_menu)
                .withMenuOpened(false)
                .inject();
        slideMenuLayout = slideMenu.getLayout();
        // initialize beginning page
        presenter.initializeBeginningPage();
        // initialize reside menu
        presenter.initializeResideMenu();
        // initialize item click
        onItemClicked();
        // initialize Fonts
        setupFonts();
        //initialize user data
        presenter.initializeUserData();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // initialize user data
        presenter.initializeUserData();
    }

    @Override
    protected void onItemClicked() {
        btnBurger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isOpened) {
                    slideMenu.openMenu();
                } else {
                    slideMenu.closeMenu();
                }
                isOpened = !isOpened;
            }
        });
        tvMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!getCurrentFragmentTag().equals(TAG_HOME)) {
                    presenter.changeFragment(imgHome, new HomeFragment(), TAG_HOME, getResources().getString(R.string.main_page));
                } else {
                    hideSideMenu();
                }
            }
        });
        tv_bank_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openSideMenuContent(MainActivity.this, BankAccountActivity.class);
            }
        });
        tvContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openSideMenuContent(MainActivity.this, ContactActivity.class);
            }
        });

        tvMyAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openSideMenuContent(MainActivity.this, MyAdsActivity.class);
            }
        });
        tvSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openSideMenuContent(MainActivity.this, SettingActivity.class);
            }
        });
        tvParticipation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openSideMenuContent(MainActivity.this, RechargeActivity.class);
            }
        });


        imgNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(MainActivity.this, NotificationActivity.class);
            }
        });
        // initialize a selection for bottom nav bar
        imgHome.setOnClickListener(this);
        imgSearch.setOnClickListener(this);
        imgAdd.setOnClickListener(this);
        imgFav.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvToolbarTitle}, this, 2);
        Fonts.setFont(new TextView[]{tvUserName, tvUserPhone, tvUserMail, tvMain, tvSetting, tvMyAds, tvParticipation, tv_bank_account, tvContact, tvLogOut}, this, 1);
    }

    @Override
    public void onClick(View view) {
        ImageView currentTab = (ImageView) view;
        switch (view.getId()) {
            case R.id.img_home:
                presenter.changeFragment(currentTab, new HomeFragment(), TAG_HOME, getResources().getString(R.string.main_page));
                break;
            case R.id.img_search:
                presenter.changeFragment(currentTab, new SearchFragment(), TAG_SEARCH, getResources().getString(R.string.search_page));
                break;
            case R.id.img_add:
                presenter.changeFragment(currentTab, new AddFragment(), TAG_ADD, getResources().getString(R.string.add_ad_page));
                break;
            case R.id.img_fav:
                presenter.changeFragment(currentTab, new FavFragment(), TAG_FAV, getResources().getString(R.string.fav_page));
                break;
            case R.id.img_profile:
                presenter.changeFragment(currentTab, new ProfileFragment(), TAG_PROFILE, getResources().getString(R.string.profile_page));
                break;
            default:
                presenter.changeFragment(currentTab, new HomeFragment(), TAG_HOME, getResources().getString(R.string.main_page));
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if (slideMenu.isMenuOpened()) {
            slideMenu.closeMenu();
        } else {
            if (!getCurrentFragmentTag().equals(TAG_HOME)) {
                presenter.changeFragment(imgHome, new HomeFragment(), TAG_HOME, getResources().getString(R.string.main_page));
            } else {
                closeActivity();
            }
        }
    }

    @Override
    public void releaseSelection(ImageView currentTab) {
        imgProfile.setPressed(false);
        imgProfile.setSelected(false);
        imgFav.setPressed(false);
        imgFav.setSelected(false);
        imgAdd.setPressed(false);
        imgAdd.setSelected(false);
        imgSearch.setPressed(false);
        imgSearch.setSelected(false);
        imgHome.setPressed(false);
        imgHome.setSelected(false);

        currentTab.setSelected(true);
    }

    @Override
    public void loadUserData(String imageProfileUrl, String userName, String phoneNum, String email) {
        Glide.with(MainActivity.this).load(BASE_IMAGE_URL + imageProfileUrl).into(imgAvatar);
        tvUserName.setText(userName);
        tvUserPhone.setText(phoneNum);
        tvUserMail.setText(email);
    }

    @Override
    public void updateToolbar(String title) {
        tvToolbarTitle.setText(title);
    }

    @Override
    public void initializeBeginningPage() {
        presenter.changeFragment(imgHome, new HomeFragment(), TAG_HOME, getResources().getString(R.string.main_page));
    }

    @Override
    public void hideSideMenu() {
        if (slideMenu.isMenuOpened()) {
            slideMenu.closeMenu();
        }
    }

    @Override
    public void initializeResideMenuViews() {
        tvUserName = slideMenuLayout.findViewById(R.id.tv_user_name);
        tvUserPhone = slideMenuLayout.findViewById(R.id.tv_user_phone);
        tvUserMail = slideMenuLayout.findViewById(R.id.tv_user_mail);
        tvMain = slideMenuLayout.findViewById(R.id.tv_main_page);
        tvSetting = slideMenuLayout.findViewById(R.id.tv_setting);
        tvMyAds = slideMenuLayout.findViewById(R.id.tv_my_ads);
        tvParticipation = slideMenuLayout.findViewById(R.id.tv_participation);
        tv_bank_account = slideMenuLayout.findViewById(R.id.tv_banck_account);
        tvContact = slideMenuLayout.findViewById(R.id.tv_contact);
        tvLogOut = slideMenuLayout.findViewById(R.id.tv_log_out);
        imgAvatar = slideMenuLayout.findViewById(R.id.img_avatar);
    }
}
