package com.extra4it.adsapp.ui.information;

import android.graphics.Bitmap;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface EditInformationMvpView extends ActivityMvpView {

    void releaseBackGround();

    void loadUSerInformation(String imageUrl, String userName, String userMail);

    void loadChooseImage(Bitmap bitmap);
}
