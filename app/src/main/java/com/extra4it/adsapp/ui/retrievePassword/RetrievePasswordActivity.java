package com.extra4it.adsapp.ui.retrievePassword;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.main.MainActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RetrievePasswordActivity extends BaseActivity implements RetrievePasswordMvpView {

    // injection
    @Inject
    RetrievePasswordMvpPresenter<RetrievePasswordMvpView> presenter;
    @BindView(R.id.tv_retrieve_password)
    TextView tvRetrievePassword;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_enter_phone)
    TextView tvEnterPhone;
    @BindView(R.id.et_user_phone)
    EditText etUserPhone;
    @BindView(R.id.tl_user_phone)
    TextInputLayout tlUserPhone;
    @BindView(R.id.btn_confirm_phone)
    Button btnConfirmPhone;
    @BindView(R.id.ln_confirm_phone)
    LinearLayout lnConfirmPhone;
    @BindView(R.id.tv_enter_code)
    TextView tvEnterCode;
    @BindView(R.id.et_enter_code)
    EditText etEnterCode;
    @BindView(R.id.tl_enter_code)
    TextInputLayout tlEnterCode;
    @BindView(R.id.tv_resend_code)
    TextView tvResendCode;
    @BindView(R.id.btn_confirm_code)
    Button btnConfirmCode;
    @BindView(R.id.ln_confirm_code)
    LinearLayout lnConfirmCode;
    @BindView(R.id.tv_account_retrieved)
    TextView tvAccountRetrieved;
    @BindView(R.id.tv_refresh_password)
    TextView tvRefreshPassword;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.tl_password)
    TextInputLayout tlPassword;
    @BindView(R.id.et_confirm_password)
    EditText etConfirmPassword;
    @BindView(R.id.tl_confirm_password)
    TextInputLayout tlConfirmPassword;
    @BindView(R.id.btn_confirm_password)
    Button btnConfirmPassword;
    @BindView(R.id.ln_confirm_password)
    LinearLayout lnConfirmPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrieve_password);
        ButterKnife.bind(this);
        // initialization
        init();
    }


    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // change statusBarColor
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // initialize item click listener
        onItemClicked();
        // initialize Fonts
        setupFonts();
    }

    @Override
    protected void onItemClicked() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });

        btnConfirmPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onConfirmPhoneClicked();
            }
        });

        btnConfirmCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onConfirmCodeClicked();
            }
        });
        btnConfirmPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onConfirmPasswordClicked();
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new Button[]{btnConfirmCode, btnConfirmPhone, btnConfirmPassword}, this, 1);
        Fonts.setFont(new TextView[]{tvEnterCode, tvEnterPhone, tvResendCode, tvRetrievePassword, tvAccountRetrieved, tvRefreshPassword}, this, 1);
        Fonts.setFont(new TextInputLayout[]{tlEnterCode, tlUserPhone, tlPassword, tlConfirmPassword}, this, 1);
        Fonts.setFont(new EditText[]{etEnterCode, etUserPhone, etPassword, etConfirmPassword}, this, 1);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    @Override
    public void confirmPhone() {
        lnConfirmPhone.setVisibility(View.GONE);
        lnConfirmCode.setVisibility(View.VISIBLE);
    }

    @Override
    public void confirmCode() {
        lnConfirmCode.setVisibility(View.GONE);
        lnConfirmPassword.setVisibility(View.VISIBLE);

    }

    @Override
    public void confirmPassword() {

    }

    @Override
    public void openMainActivity() {
        Intent intent = new Intent(RetrievePasswordActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
