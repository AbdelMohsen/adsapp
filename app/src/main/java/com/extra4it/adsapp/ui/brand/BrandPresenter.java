package com.extra4it.adsapp.ui.brand;

import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BrandPresenter<V extends BrandMvpView> extends BasePresenter<V>
        implements BrandMvpPresenter<V> {

    @Inject
    public BrandPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void loadBrandAds(final ProgressBar progressBar, int subSectionId) {
        getMvpView().showLoading(progressBar);
        getDataManager().getApiHelper().getBrandAds(subSectionId)
                .enqueue(new Callback<BrandResponse>() {
                    @Override
                    public void onResponse(Call<BrandResponse> call, Response<BrandResponse> response) {
                        getMvpView().hideLoading(progressBar);
                        getMvpView().showBrandAds(response);
                    }

                    @Override
                    public void onFailure(Call<BrandResponse> call, Throwable t) {
                        getMvpView().hideLoading(progressBar);

                    }
                });
    }

    @Override
    public void loadBrandAds(final ProgressBar progressBar, int subSectionId, int cityId) {
        getMvpView().showLoading(progressBar);
        getDataManager().getApiHelper().getBrandAds(subSectionId, cityId)
                .enqueue(new Callback<BrandResponse>() {
                    @Override
                    public void onResponse(Call<BrandResponse> call, Response<BrandResponse> response) {
                        getMvpView().showBrandAds(response);
                        getMvpView().hideLoading(progressBar);
                    }

                    @Override
                    public void onFailure(Call<BrandResponse> call, Throwable t) {
                        getMvpView().hideLoading(progressBar);

                    }
                });
    }
}
