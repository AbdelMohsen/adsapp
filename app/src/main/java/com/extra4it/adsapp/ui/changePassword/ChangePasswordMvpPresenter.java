package com.extra4it.adsapp.ui.changePassword;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface ChangePasswordMvpPresenter<V extends ChangePasswordMvpView> extends MvpPresenter<V> {

    void onChangePassword(ProgressBar progressBar, View view, EditText etNewPassword, EditText etConfirmNewPassword,
                          EditText etOldPassword);
}
