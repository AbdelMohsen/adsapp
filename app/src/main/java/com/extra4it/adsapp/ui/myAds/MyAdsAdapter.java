package com.extra4it.adsapp.ui.myAds;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.AppConstants;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MyAdsAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private Context context;
    private List<BrandResponse.Data> adList = new ArrayList<>();
    private LayoutInflater inflater;
    private BrandClickCallBack brandCallBack;

    public MyAdsAdapter(Context context, List<BrandResponse.Data> adList) {
        this.context = context;
        this.adList = adList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_brand, parent, false);
                return new MyAdsViewHolder(normalView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        if (adList != null && adList.size() > 0) {
            return adList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (adList != null && adList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public void setBrandCallBack(BrandClickCallBack brandCallBack) {
        this.brandCallBack = brandCallBack;
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class MyAdsViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.img_brand)
        ImageView imgBrand;
        @BindView(R.id.tv_ad_description)
        TextView tvAdDescription;
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.tv_date)
        TextView tvDate;

        public MyAdsViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvDate, tvUserName}, context, 1);
            Fonts.setFont(new TextView[]{tvAdDescription}, context, 2);

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            Glide.with(context).load(AppConstants.BASE_IMAGE_URL + adList.get(position).getImg()).into(imgBrand);
//            tvUserName.setText(adList.get(position).getUser().get(0).getName());
            tvAdDescription.setText(adList.get(position).getTitle());
            tvDate.setText(adList.get(position).getCreated_at());
        }

        @Override
        public void onClick(View view) {
            if (brandCallBack != null) {
                brandCallBack.onBrandClicked(getAdapterPosition());
            }
        }
    }

    public interface BrandClickCallBack {
        void onBrandClicked(int position);
    }
}
