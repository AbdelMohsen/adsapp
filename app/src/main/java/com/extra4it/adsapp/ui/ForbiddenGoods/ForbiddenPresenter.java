package com.extra4it.adsapp.ui.ForbiddenGoods;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class ForbiddenPresenter<V extends ForbiddenMvpView> extends BasePresenter<V>
        implements ForbiddenMvpPresenter<V> {


    @Inject
    public ForbiddenPresenter(DataManager dataManager) {
        super(dataManager);
    }
}
