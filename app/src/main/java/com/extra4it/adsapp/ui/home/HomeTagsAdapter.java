package com.extra4it.adsapp.ui.home;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.HomeTagsResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.Fonts;

import java.util.List;

import butterknife.BindView;

public class HomeTagsAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private Context context;
    private List<HomeTagsResponse.Data> homeTagsList;
    private LayoutInflater inflater;
    private int rowIndex = 0;
    private TagSelected tagSelected;

    public HomeTagsAdapter(Context context, List<HomeTagsResponse.Data> homeTagsList) {
        this.context = context;
        this.homeTagsList = homeTagsList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new HomeTagsViewHolder(
                        inflater.inflate(R.layout.row_home_tags, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new HomeTagsViewHolder(
                        inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        if (homeTagsList != null && homeTagsList.size() != 0) {
            return homeTagsList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (homeTagsList != null && homeTagsList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }

    }

    public void setOnTagSelectedListener(TagSelected tagSelectedListener) {
        this.tagSelected = tagSelectedListener;
    }

    class EmptyTagsViewHolder extends BaseViewHolder {
        @BindView(R.id.tv_retry)
        TextView tvRetry;

        public EmptyTagsViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvRetry}, context, 2);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
        }
    }


    class HomeTagsViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_home_tag)
        TextView tvHomeTag;
        View view;

        public HomeTagsViewHolder(View itemView) {
            super(itemView);
            tvHomeTag.setOnClickListener(this);
            this.view = itemView;
        }

        @Override
        public void onBind(final int position) {
            super.onBind(position);
            try {
                Resources r = context.getResources();
                int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 140, r.getDisplayMetrics()); // i have bottom tabbar so yf you dont have any thing like this just leave 150 to 0.I think in your case height of image view an your top(Pifer)
                //this change width of rcv
                DisplayMetrics displaymetrics = new DisplayMetrics();
                ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                int width = displaymetrics.widthPixels;
                view.setMinimumWidth(width / 4);
                tvHomeTag.setMinimumWidth(width / 4);
                tvHomeTag.setText(homeTagsList.get(position).getCat_name());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (rowIndex == position) {
                tvHomeTag.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                tvHomeTag.setTextColor(context.getResources().getColor(R.color.colorWhite));
            } else {
                tvHomeTag.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                tvHomeTag.setTextColor(context.getResources().getColor(R.color.colorBlack));
            }

        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvHomeTag}, context, 1);
        }


        @Override
        public void onClick(View view) {
            rowIndex = getAdapterPosition();
            notifyDataSetChanged();
            if (tagSelected != null) {
                tagSelected.onTagSelected(rowIndex);
            }
        }
    }

    public interface TagSelected {
        void onTagSelected(int position);
    }
}
