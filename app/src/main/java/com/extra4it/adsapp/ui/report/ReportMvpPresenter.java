package com.extra4it.adsapp.ui.report;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface ReportMvpPresenter<V extends ReportMvpView> extends MvpPresenter<V> {

    void addReport(ProgressBar progressBar, View view, EditText etReport,int adId);

}
