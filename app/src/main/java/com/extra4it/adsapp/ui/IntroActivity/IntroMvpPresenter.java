package com.extra4it.adsapp.ui.IntroActivity;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface IntroMvpPresenter<V extends IntroMvpView> extends MvpPresenter<V> {
    void onNextClicked();

    void onSkipClicked();
}
