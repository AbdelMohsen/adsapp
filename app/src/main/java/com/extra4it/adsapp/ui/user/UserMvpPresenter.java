package com.extra4it.adsapp.ui.user;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface UserMvpPresenter<V extends UserMvpView> extends MvpPresenter<V> {
}
