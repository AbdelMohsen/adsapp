package com.extra4it.adsapp.ui.home;

import com.extra4it.adsapp.data.network.model.HomeTagsResponse;
import com.extra4it.adsapp.data.network.model.SectionResponse;
import com.extra4it.adsapp.ui.base.MvpView;

import retrofit2.Response;

public interface HomeMvpView extends MvpView {

    void showHomeTags(Response<HomeTagsResponse> response);

    void showSubSections(Response<SectionResponse> response);
}
