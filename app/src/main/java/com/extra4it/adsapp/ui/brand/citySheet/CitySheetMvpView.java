package com.extra4it.adsapp.ui.brand.citySheet;

import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.ui.base.MvpView;

import retrofit2.Response;

public interface CitySheetMvpView extends MvpView {
    void showCities(Response<CityResponse> response);
}
