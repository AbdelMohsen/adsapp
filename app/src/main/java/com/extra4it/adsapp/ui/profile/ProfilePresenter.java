package com.extra4it.adsapp.ui.profile;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.UserProfileResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePresenter<V extends ProfileMvpView> extends BasePresenter<V>
        implements ProfileMvpPresenter<V> {

    @Inject
    public ProfilePresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void getUserData() {
        getDataManager().getApiHelper().getUserData("Bearer " + getDataManager().getAccessToken())
                .enqueue(new Callback<UserProfileResponse>() {
                    @Override
                    public void onResponse(Call<UserProfileResponse> call, Response<UserProfileResponse> response) {
                        getMvpView().loadUserData(response);
                    }

                    @Override
                    public void onFailure(Call<UserProfileResponse> call, Throwable t) {

                    }
                });
    }
}
