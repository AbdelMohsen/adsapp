package com.extra4it.adsapp.ui.splash;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface SplashMvpView extends ActivityMvpView {

    void openIntroActivity();

    void openLoginActivity();

    void openMainActivity();
}
