package com.extra4it.adsapp.ui.base;

import android.content.Context;
import android.support.v4.app.Fragment;

import java.util.HashMap;

/**
 * Created by user on 03/06/2018.
 */

public interface ActivityMvpView extends MvpView {
    void closeActivity();

    Context getContext();

    void changeStatusBarColor(int color);

    void loadCurrentFragment(Fragment fragment, String TAG);

    void hideStatusBar();


}
