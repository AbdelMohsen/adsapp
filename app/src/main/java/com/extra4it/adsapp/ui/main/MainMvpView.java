package com.extra4it.adsapp.ui.main;

import android.widget.ImageView;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface MainMvpView extends ActivityMvpView {
    void updateToolbar(String title);

    void initializeBeginningPage();

    void hideSideMenu();

    void initializeResideMenuViews();

    void releaseSelection(ImageView currentTab);

    void loadUserData(String imageProfileUrl, String userName, String phoneNum, String email);


}
