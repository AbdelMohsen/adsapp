package com.extra4it.adsapp.ui.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.changePassword.ChangePasswordActivity;
import com.extra4it.adsapp.ui.changePhone.ChangePhoneActivity;
import com.extra4it.adsapp.ui.information.EditInformationActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingActivity extends BaseActivity implements SettingMvpView {

    @Inject
    SettingMvpPresenter<SettingMvpView> presenter;
    @BindView(R.id.tv_setting)
    TextView tvSetting;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_edit_data)
    TextView tvEditData;
    @BindView(R.id.tv_edit_phone)
    TextView tvEditPhone;
    @BindView(R.id.tv_edit_password)
    TextView tvEditPassword;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // change statusBar color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // initialize clickListener
        onItemClicked();
        // initialize fonts
        setupFonts();
    }

    @Override
    protected void onItemClicked() {
        tvEditData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(SettingActivity.this, EditInformationActivity.class);
            }
        });
        tvEditPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(SettingActivity.this, ChangePhoneActivity.class);

            }
        });
        tvEditPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity(SettingActivity.this, ChangePasswordActivity.class);

            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvEditData, tvEditPassword, tvEditPhone, tvSetting}, this, 2);
    }
}
