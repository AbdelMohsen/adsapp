package com.extra4it.adsapp.ui.base;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.di.component.ActivityComponent;

/**
 * Created by user on 31/05/2018.
 */

public abstract class BaseDialog extends android.support.v4.app.DialogFragment implements DialogMvpView {


    private BaseActivity baseActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) context;
            this.baseActivity = baseActivity;
            baseActivity.onFragmentAttached();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.MyDialogAnimation;
    }

    @Override
    public void dismissDialog(String tag) {
        dismiss();
        baseActivity.onFragmentDetached(tag);
    }

    @Override
    public void showLoading(ProgressBar progressBar) {
        if (baseActivity != null) {
            baseActivity.showLoading(progressBar);
        }
    }

    @Override
    public void hideLoading(ProgressBar progressBar) {
        baseActivity.hideLoading(progressBar);
    }

    @Override
    public void showMessage(View view, String message) {
        baseActivity.showMessage(view, message);
    }

    @Override
    public void hideKeyboard() {
        baseActivity.hideKeyboard();
    }

    @Override
    public boolean inNetworkConnected() {
        return false;
    }

    @Override
    public void showSnackBar(View view, String message) {
        baseActivity.showSnackBar(view, message);
    }


    @Override
    public void onDetach() {
        baseActivity = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setup(view);
        setupFont();
        onItemClick();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    @Override
    public Dialog fullScreenDialog() {
        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getContext(), R.style.MyDialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        return dialog;
    }

    protected abstract void setup(View view);

    protected abstract void setupFont();

    protected abstract void onItemClick();

    public BaseActivity getBaseActivity() {
        return baseActivity;
    }

    public ActivityComponent getActivityComponent() {
        return baseActivity.getActivityComponent();
    }

    @Override
    public void openActivity(Context context, Class<?> cls) {

    }
}
