package com.extra4it.adsapp.ui.add;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class AddPresenter<V extends AddMvpView> extends BasePresenter<V>
        implements AddMvpPresenter<V> {

    @Inject
    public AddPresenter(DataManager dataManager) {
        super(dataManager);
    }
}
