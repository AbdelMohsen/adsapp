package com.extra4it.adsapp.ui.myAds;

import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface MyAdsMvpPresenter<V extends MyAdsMvpView> extends MvpPresenter<V> {

    void getUserAds(ProgressBar progressBar);
}
