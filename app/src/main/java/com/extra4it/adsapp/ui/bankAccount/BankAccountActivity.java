package com.extra4it.adsapp.ui.bankAccount;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.BankAccountResponse;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class BankAccountActivity extends BaseActivity implements BankAccountMvpView {
    // injection
    @Inject
    BankAccountMvpPresenter<BankAccountMvpView> presenter;
    // widgets
    @BindView(R.id.tv_bank_account)
    TextView tvBankAccount;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_inner_bank_account)
    TextView tvInnerBankAccount;
    @BindView(R.id.tv_bank_description)
    TextView tvBankDescription;
    @BindView(R.id.rv_bank_accounts)
    RecyclerView rvBankAccounts;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_account);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize click listener
        onItemClicked();
        // initialize Fonts
        setupFonts();
        // change statusBar color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // initialize bank account data
        presenter.getBankAccounts();

    }

    @Override
    protected void onItemClicked() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvBankDescription}, this, 1);
        Fonts.setFont(new TextView[]{tvBankAccount, tvInnerBankAccount}, this, 2);
    }

    @Override
    public void loadBanksData(Response<BankAccountResponse> response) {
        List<BankAccountResponse.Banks> bankAccountList = response.body().getData().getBanks();
        // initialize recyclerView
        rvBankAccounts.setLayoutManager(new LinearLayoutManager(this));
        BankAccountAdapter bankAccountAdapter = new BankAccountAdapter(this, bankAccountList);
        rvBankAccounts.setAdapter(bankAccountAdapter);

        tvBankDescription.setText(response.body().getData().getText());
    }
}
