package com.extra4it.adsapp.ui.retrievePassword;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class RetrievePasswordPresenter<V extends RetrievePasswordMvpView> extends BasePresenter<V>
        implements RetrievePasswordMvpPresenter<V> {

    @Inject
    public RetrievePasswordPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onConfirmPhoneClicked() {
        getMvpView().confirmPhone();
    }

    @Override
    public void onConfirmCodeClicked() {
        getMvpView().confirmCode();
    }

    @Override
    public void onConfirmPasswordClicked() {
        getMvpView().confirmPassword();
        getMvpView().openMainActivity();
    }
}
