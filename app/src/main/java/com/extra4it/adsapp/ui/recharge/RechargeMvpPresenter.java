package com.extra4it.adsapp.ui.recharge;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface RechargeMvpPresenter<V extends RechargeMvpView> extends MvpPresenter<V> {
}
