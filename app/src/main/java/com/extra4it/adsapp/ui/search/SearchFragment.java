package com.extra4it.adsapp.ui.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.data.network.model.HomeTagsResponse;
import com.extra4it.adsapp.data.network.model.SectionResponse;
import com.extra4it.adsapp.ui.base.BaseFragment;
import com.extra4it.adsapp.ui.brand.citySheet.CityAdapter;
import com.extra4it.adsapp.ui.searchResult.SearchResultActivity;
import com.extra4it.adsapp.utils.Fonts;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

public class SearchFragment extends BaseFragment implements SearchMvpView, CityAdapter.CityCallBack, SectionsAdapter.SectionCallBack, SubSectionAdapter.SubSectionCallBack {
    @Inject
    SearchMvpPresenter<SearchMvpView> presenter;
    @BindView(R.id.tv_specialize_search)
    TextView tvSpecializeSearch;
    @BindView(R.id.tv_city)
    TextView tvCity;
    @BindView(R.id.tv_choose)
    TextView tvChoose;
    @BindView(R.id.search_container)
    LinearLayout searchContainer;
    Unbinder unbinder;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.img_arrow)
    ImageView imgArrow;
    @BindView(R.id.ln_show_hide_search)
    LinearLayout lnShowHideSearch;
    @BindView(R.id.btn_search)
    Button btnSearch;
    @BindView(R.id.rv_city)
    RecyclerView rvCity;
    @BindView(R.id.tv_classification)
    TextView tvClassification;
    @BindView(R.id.tv_choose1)
    TextView tvChoose1;
    @BindView(R.id.tv_sections)
    TextView tvSections;
    @BindView(R.id.rv_sections)
    RecyclerView rvSections;
    @BindView(R.id.tv_sub_sections)
    TextView tvSubSections;
    @BindView(R.id.rv_sub_sections)
    RecyclerView rvSubSections;
    private List<CityResponse.Data> cityList;
    private Integer currentCityId = null;
    private Integer currentSectionId = -1;
    private Integer currentSubSectionId = -1;
    private List<HomeTagsResponse.Data> sectionList;
    private List<SectionResponse.Data> subSectionList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onItemClicked() {
        lnShowHideSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // show and hide advanced search
                presenter.onShowHideSearch();

            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SearchResultActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_up_out, R.anim.fade_out);

            }
        });
    }

    @Override
    protected void setup(View view) {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);


        // show advanced search data
        presenter.loadCityData();
        presenter.loadSections();

    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvChoose1, tvClassification, tvSections, tvSubSections, tvChoose, tvCity, tvSpecializeSearch}, getActivity(), 2);
        Fonts.setFont(new EditText[]{etSearch}, getActivity(), 1);
        Fonts.setFont(new Button[]{btnSearch}, getActivity(), 1);
    }

    @Override
    public void openActivity(Context context, Class<?> cls) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showHideSearchContainer() {
        imgArrow.setBackground(null);
        if (searchContainer.getVisibility() == View.GONE) {
            searchContainer.setVisibility(View.VISIBLE);
            imgArrow.setBackground(getResources().getDrawable(R.drawable.expand_arrow));
            tvSpecializeSearch.setTextColor(getResources().getColor(R.color.colorPrimary));
            imgArrow.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.expand_arrow));
        } else {
            searchContainer.setVisibility(View.GONE);
            imgArrow.setBackground(getResources().getDrawable(R.drawable.collaps_arrow));
            tvSpecializeSearch.setTextColor(getResources().getColor(R.color.colorBlack));
            imgArrow.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.collaps_arrow));
        }

    }

    @Override
    public void showCities(Response<CityResponse> response) {
        cityList = response.body().getData();
        // initialize rv_city

        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(getActivity());
        // Set flex direction.
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        // Set JustifyContent.
        flexboxLayoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);
        rvCity.setLayoutManager(flexboxLayoutManager);
        CityAdapter cityAdapter = new CityAdapter(getActivity(), cityList);
        cityAdapter.setCityCallBack(this);
        rvCity.setAdapter(cityAdapter);
    }

    @Override
    public void showSections(Response<HomeTagsResponse> response) {
        sectionList = response.body().getData();
        // initialize rv_sections

        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(getActivity());
        // Set flex direction.
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        // Set JustifyContent.
        flexboxLayoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);
        rvSections.setLayoutManager(flexboxLayoutManager);
        SectionsAdapter sectionsAdapter = new SectionsAdapter(getActivity(), sectionList);
        sectionsAdapter.setSectionCallBack(this);
        rvSections.setAdapter(sectionsAdapter);

    }

    @Override
    public void showSubSection(Response<SectionResponse> response) {
        subSectionList = response.body().getData();

        // initialize rv_sections

        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(getActivity());
        // Set flex direction.
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        // Set JustifyContent.
        flexboxLayoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);
        rvSubSections.setLayoutManager(flexboxLayoutManager);
        SubSectionAdapter subSectionAdapter = new SubSectionAdapter(getActivity(), subSectionList);
        subSectionAdapter.setSubSectionCallBack(this);
        rvSubSections.setAdapter(subSectionAdapter);
    }

    @Override
    public void onCityClick(int position) {
        currentCityId = cityList.get(position).getId();
        Log.e("xxx", "city id" + currentCityId);
    }

    @Override
    public void onSectionClick(int position) {
        currentSectionId = sectionList.get(position).getId();
        Log.e("xxx", "section id" + currentSectionId);
        presenter.loadSubSections(currentSectionId);

    }

    @Override
    public void onSubSectionClick(int position) {
        currentSubSectionId = sectionList.get(position).getId();
        Log.e("xxx", "section id" + currentSubSectionId);
    }
}
