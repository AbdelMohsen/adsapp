package com.extra4it.adsapp.ui.ForbiddenGoods;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface ForbiddenMvpPresenter<V extends ForbiddenMvpView> extends MvpPresenter<V> {
}
