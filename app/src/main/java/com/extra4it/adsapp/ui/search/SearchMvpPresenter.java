package com.extra4it.adsapp.ui.search;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface SearchMvpPresenter<V extends SearchMvpView> extends MvpPresenter<V> {
    void onShowHideSearch();

    void loadCityData();

    void loadSections();

    void loadSubSections(int sectionId);
}
