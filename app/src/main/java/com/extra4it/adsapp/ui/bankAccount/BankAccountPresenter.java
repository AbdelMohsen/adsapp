package com.extra4it.adsapp.ui.bankAccount;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.BankAccountResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankAccountPresenter<V extends BankAccountMvpView> extends BasePresenter<V>
        implements BankAccountMvpPresenter<V> {

    @Inject
    public BankAccountPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void getBankAccounts() {
        getDataManager().getApiHelper().getBankAccounts().enqueue(new Callback<BankAccountResponse>() {
            @Override
            public void onResponse(Call<BankAccountResponse> call, Response<BankAccountResponse> response) {
                if (response.body().getStatus() == true) {
                    getMvpView().loadBanksData(response);
                }
            }

            @Override
            public void onFailure(Call<BankAccountResponse> call, Throwable t) {

            }
        });
    }
}
