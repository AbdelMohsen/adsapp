package com.extra4it.adsapp.ui.ForbiddenGoods;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForbiddenActivity extends BaseActivity implements ForbiddenMvpView {
    // injection
    @Inject
    ForbiddenMvpPresenter<ForbiddenMvpView> presenter;
    // Widgets
    @BindView(R.id.tv_forbidden)
    TextView tvForbidden;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_what_forbidden)
    TextView tvWhatForbidden;
    @BindView(R.id.tv_forbidden_description)
    TextView tvForbiddenDescription;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forbidden);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize clickListener
        onItemClicked();
        // initialize fonts
        setupFonts();
    }

    @Override
    protected void onItemClicked() {

    }

    @Override
    protected void setupFonts() {

    }
}
