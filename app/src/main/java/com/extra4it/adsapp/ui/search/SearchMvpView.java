package com.extra4it.adsapp.ui.search;

import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.data.network.model.HomeTagsResponse;
import com.extra4it.adsapp.data.network.model.SectionResponse;
import com.extra4it.adsapp.ui.base.MvpView;

import retrofit2.Response;

public interface SearchMvpView extends MvpView {
    void showHideSearchContainer();

    void showCities(Response<CityResponse> response);

    void showSections(Response<HomeTagsResponse> response);

    void showSubSection(Response<SectionResponse> response);
}
