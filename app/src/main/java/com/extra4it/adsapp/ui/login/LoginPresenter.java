package com.extra4it.adsapp.ui.login;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.LoginResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.Validation;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V>
        implements LoginMvpPresenter<V> {

    @Inject
    public LoginPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onNewRegisterClicked() {
        getMvpView().openRegisterActivity();
    }

    @Override
    public void onRetrievePasswordClicked() {
        getMvpView().openRetrievePasswordActivity();
    }

    @Override
    public void onLoginButtonClicked(final ProgressBar progressBar, final View view, EditText etUserName, EditText etPassword) {
        if (Validation.internetConnectionAvailable()) {
            if (Validation.validate(getMvpView().getContext(), new EditText[]{etUserName, etPassword})) {
                if (Validation.isPasswordValid(getMvpView().getContext(), etPassword)) {

                    getMvpView().showLoading(progressBar);

                    getDataManager().getApiHelper().login(etUserName.getText().toString(), etPassword.getText().toString())
                            .enqueue(new Callback<LoginResponse>() {
                                @Override
                                public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                                    getMvpView().hideLoading(progressBar);
                                    Log.e("xxx","Login body");

                                        if (response.body().getStatus() == true) {
                                            getMvpView().openMainActivity();
                                            getDataManager().setCurrentUserLoggedInMode(DataManager.LoggedInMode.LOGGED_IN_MODE_SERVER);
                                            getDataManager().SetAccessToken(response.body().getAccess_token());
                                            getDataManager().setCurrentUserId(response.body().getData().getId());
                                            getDataManager().setCurrentUserName(response.body().getData().getName());
                                            getDataManager().setCurrentUserEmail(response.body().getData().getEmail());
                                            getDataManager().setCurrentUserProfilePicURL(response.body().getData().getImg());
                                            getDataManager().setCurrentUserPhoneNum(response.body().getData().getMobile());
                                        } else {
                                            getMvpView().showMessage(view, "معلومات الدخول غير صحيحه");
                                        }
                                    }

                                @Override
                                public void onFailure(Call<LoginResponse> call, Throwable t) {
                                    getMvpView().hideLoading(progressBar);
                                    Log.e("xxx","failure body : "+t);

                                }
                            });
                }
            }
        } else {
            getMvpView().showSnackBar(view, "من فضلك تحقق من الانترنت ");
        }
    }
}
