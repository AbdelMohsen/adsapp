package com.extra4it.adsapp.ui.confirmPay;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseDialog;
import com.extra4it.adsapp.utils.Fonts;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ConfirmDialog extends BaseDialog {
    @BindView(R.id.tv_order_sent)
    TextView tvOrderSent;
    @BindView(R.id.tv_order_confirmation)
    TextView tvOrderConfirmation;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    Unbinder unbinder;

    // vars
    OnConfirmCallback confirmCallback;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_confirm, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnConfirmCallback) {
            confirmCallback = (OnConfirmCallback) context;
        }
    }

    @Override
    protected void setup(View view) {

    }

    @Override
    protected void setupFont() {
        Fonts.setFont(new TextView[]{tvContinue, tvOrderConfirmation}, getActivity(), 1);
        Fonts.setFont(new TextView[]{tvOrderSent}, getActivity(), 2);

    }

    @Override
    protected void onItemClick() {
        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (confirmCallback != null) {
                    confirmCallback.onConfirmClick();
                    dismiss();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface OnConfirmCallback {
        void onConfirmClick();
    }
}
