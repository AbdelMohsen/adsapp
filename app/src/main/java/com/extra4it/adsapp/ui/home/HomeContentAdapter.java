package com.extra4it.adsapp.ui.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.SectionResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.AppConstants;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class HomeContentAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private Context context;
    List<SectionResponse.Data> subSectionList = new ArrayList<>();
    private LayoutInflater inflater;
    private HomeContentCallBack homeContentCallBack;

    public HomeContentAdapter(Context context, List<SectionResponse.Data> subSectionList) {
        this.context = context;
        this.subSectionList = subSectionList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_home_result_content, parent, false);
                return new HomeContentViewHolder(normalView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        if (subSectionList != null && subSectionList.size() > 0) {
            return subSectionList.size();
        } else {
            return 1;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (subSectionList != null && subSectionList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public void setHomeContentClicked(HomeContentCallBack homeContentCallBack) {
        this.homeContentCallBack = homeContentCallBack;
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class HomeContentViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.img_brand)
        ImageView imgBrand;
        @BindView(R.id.tv_brand)
        TextView tvBrand;
        @BindView(R.id.card_content)
        CardView cardContent;

        public HomeContentViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvBrand}, context, 1);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            Glide.with(context).load(AppConstants.BASE_IMAGE_URL + subSectionList.get(position).getSub_img()).into(imgBrand);
            tvBrand.setText(subSectionList.get(position).getSub_name());
        }

        @Override
        public void onClick(View view) {
            if (homeContentCallBack != null) {
                homeContentCallBack.onSubSectionsClicked(getAdapterPosition());
            } else {
                Log.e("xxx", "Listener is null");
            }
        }
    }

    public interface HomeContentCallBack {
        void onSubSectionsClicked(int position);
    }
}
