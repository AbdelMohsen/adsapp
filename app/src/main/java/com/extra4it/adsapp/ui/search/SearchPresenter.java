package com.extra4it.adsapp.ui.search;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.data.network.model.HomeTagsResponse;
import com.extra4it.adsapp.data.network.model.SectionResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchPresenter<V extends SearchMvpView> extends BasePresenter<V>
        implements SearchMvpPresenter<V> {

    @Inject
    public SearchPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onShowHideSearch() {
        getMvpView().showHideSearchContainer();
    }

    @Override
    public void loadCityData() {
        getDataManager().getApiHelper().getCities().enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                getMvpView().showCities(response);
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void loadSections() {
        getDataManager().getApiHelper().getTags()
                .enqueue(new Callback<HomeTagsResponse>() {
                    @Override
                    public void onResponse(Call<HomeTagsResponse> call, Response<HomeTagsResponse> response) {
                        if (response.body().getStatus() == true) {
                            getMvpView().showSections(response);
                        }
                    }

                    @Override
                    public void onFailure(Call<HomeTagsResponse> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadSubSections(int SectionId) {
        getDataManager().getApiHelper().getSubSections(SectionId)
                .enqueue(new Callback<SectionResponse>() {
                    @Override
                    public void onResponse(Call<SectionResponse> call, Response<SectionResponse> response) {
                        getMvpView().showSubSection(response);
                    }

                    @Override
                    public void onFailure(Call<SectionResponse> call, Throwable t) {

                    }
                });
    }
}
