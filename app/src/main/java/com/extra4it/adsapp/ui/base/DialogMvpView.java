package com.extra4it.adsapp.ui.base;

import android.app.Dialog;

/**
 * Created by user on 31/05/2018.
 */

public interface DialogMvpView extends MvpView {
    void dismissDialog(String tag);

    Dialog fullScreenDialog();
}
