package com.extra4it.adsapp.ui.contact;

import android.view.View;
import android.widget.EditText;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface ContactMvpPresenter<V extends ContactMvpView> extends MvpPresenter<V> {
    void contact(View view, EditText editText);
}
