package com.extra4it.adsapp.ui.likes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.LikesResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class LikesAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;

    private Context context;
    private List<LikesResponse> likesList = new ArrayList<>();
    private LayoutInflater inflater;
    private LikesClickCallBack likesClickCallBack;

    public LikesAdapter(Context context, List<LikesResponse> likesList) {
        this.context = context;
        this.likesList = likesList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_likes, parent, false);
                return new SpecialPostsViewHolder(normalView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    @Override
    public int getItemViewType(int position) {
//        if (likesList != null && likesList.size() > 0) {
//            return VIEW_TYPE_NORMAL;
//        } else {
//            return VIEW_TYPE_EMPTY;
//        }
        return 1;
    }

    public void setLikesClickCallBack(LikesClickCallBack likesClickCallBack) {
        this.likesClickCallBack = likesClickCallBack;
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class SpecialPostsViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.img_avatar)
        CircleImageView imgAvatar;
        @BindView(R.id.tv_user_name)
        TextView tvUserName;

        public SpecialPostsViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvUserName}, context, 1);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
        }

        @Override
        public void onClick(View view) {
            if (likesClickCallBack != null) {
                likesClickCallBack.onUserClickListener(view, getAdapterPosition());
            }
        }
    }

    public interface LikesClickCallBack {
        void onUserClickListener(View view, int position);
    }
}
