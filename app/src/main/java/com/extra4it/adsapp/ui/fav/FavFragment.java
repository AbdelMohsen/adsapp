package com.extra4it.adsapp.ui.fav;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.advertisment.AdvActivity;
import com.extra4it.adsapp.ui.base.BaseFragment;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

public class FavFragment extends BaseFragment implements FavMvpView, FavAdapter.BrandClickCallBack {
    @Inject
    FavMvpPresenter<FavMvpView> presenter;
    @BindView(R.id.rv_fav)
    RecyclerView rvFav;
    Unbinder unbinder;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;
    private List<BrandResponse.Data> brandList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fav, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onItemClicked() {

    }

    @Override
    protected void setup(View view) {
        // injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // load user Fav data
        presenter.loadUserFavData(progressBar);

    }

    @Override
    protected void setupFonts() {

    }

    @Override
    public void openActivity(Context context, Class<?> cls) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onBrandClicked(int position) {
        Intent intent = new Intent(getActivity(), AdvActivity.class);
        intent.putExtra("adId", brandList.get(position).getId());
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.push_up_out, R.anim.fade_out);
    }

    @Override
    public void showUserFavData(Response<BrandResponse> response) {
        // initialize fav recyclerView
        brandList = response.body().getData();
        rvFav.setLayoutManager(new LinearLayoutManager(getActivity()));
        FavAdapter favAdapter = new FavAdapter(getActivity(), brandList);
        favAdapter.setBrandCallBack(this);
        rvFav.setAdapter(favAdapter);
    }
}
