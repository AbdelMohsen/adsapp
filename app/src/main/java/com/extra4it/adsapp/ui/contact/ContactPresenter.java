package com.extra4it.adsapp.ui.contact;

import android.view.View;
import android.widget.EditText;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.model.DefaultResponse;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.Validation;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactPresenter<V extends ContactMvpView> extends BasePresenter<V>
        implements ContactMvpPresenter<V> {

    @Inject
    public ContactPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void contact(final View view, final EditText editText) {
        if (Validation.validate(getMvpView().getContext(), new EditText[]{editText})) {
            getDataManager().getApiHelper().contact("Bearer " + getDataManager().getAccessToken(), editText.getText().toString())
                    .enqueue(new Callback<DefaultResponse>() {
                        @Override
                        public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                            if (response.body() != null) {
                                getMvpView().showMessage(view, response.body().getMsg());
                                getMvpView().hideKeyboard();
                                getMvpView().clearData(editText);
                            }
                        }

                        @Override
                        public void onFailure(Call<DefaultResponse> call, Throwable t) {

                        }
                    });
        }
    }
}
