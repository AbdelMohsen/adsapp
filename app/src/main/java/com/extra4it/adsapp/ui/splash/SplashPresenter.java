package com.extra4it.adsapp.ui.splash;

import android.os.Handler;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import javax.inject.Inject;

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V>
        implements SplashMvpPresenter<V> {

    @Inject
    public SplashPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onOpenDecideActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getMvpView().openIntroActivity();
            }
        }, 500);
    }
}
