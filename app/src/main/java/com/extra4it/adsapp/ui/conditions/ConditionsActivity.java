package com.extra4it.adsapp.ui.conditions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConditionsActivity extends BaseActivity implements ConditionsMvpView {
    // injection
    @Inject
    ConditionsMvpPresenter<ConditionsMvpView> presenter;
    @BindView(R.id.tv_condition)
    TextView tvCondition;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_conditions)
    TextView tvConditions;
    @BindView(R.id.tv_condition_status)
    TextView tvConditionStatus;
    // widgets

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conditions);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize clickListener
        onItemClicked();
        // initialize fonts
        setupFonts();
        // change statusBar color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
    }

    @Override
    protected void onItemClicked() {

    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvConditions, tvConditionStatus}, this, 1);
        Fonts.setFont(new TextView[]{tvCondition}, this, 2);

    }

    @Override
    public void showConditions(String terms) {
        tvConditionStatus.setText(terms);
    }
}
