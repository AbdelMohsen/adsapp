package com.extra4it.adsapp.ui.base;


import com.extra4it.adsapp.data.DataManager;

import javax.inject.Inject;

/**
 * Created by user on 16/05/2018.
 */

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private V mvpView;
    private DataManager dataManager;

    @Inject
    public BasePresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void onAttach(V mvpView) {
        this.mvpView = mvpView;
    }

    @Override
    public void onDetach() {
        this.mvpView = null;
    }


    public DataManager getDataManager() {
        return dataManager;
    }

    public V getMvpView() {
        return mvpView;
    }

    // not sure why use it
    @Override
    public void setUserAsLoggedOut() {
        getDataManager().SetAccessToken(null);
    }



}
