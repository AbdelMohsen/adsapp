package com.extra4it.adsapp.ui.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.UserProfileResponse;
import com.extra4it.adsapp.ui.base.BaseFragment;
import com.extra4it.adsapp.utils.AppConstants;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment implements ProfileMvpView {
    @Inject
    ProfileMvpPresenter<ProfileMvpView> presenter;
    @BindView(R.id.img_user_profile)
    ImageView imgUserProfile;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_user_address)
    TextView tvUserAddress;
    @BindView(R.id.tv_user_info)
    TextView tvUserInfo;
    @BindView(R.id.tv_user_phone)
    TextView tvUserPhone;
    @BindView(R.id.tv_user_phone_status)
    TextView tvUserPhoneStatus;
    @BindView(R.id.tv_user_mail)
    TextView tvUserMail;
    @BindView(R.id.tv_user_mail_status)
    TextView tvUserMailStatus;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void onItemClicked() {

    }

    @Override
    protected void setup(View view) {
        // injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);

        presenter.getUserData();

    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvUserAddress, tvUserInfo, tvUserMail, tvUserMailStatus, tvUserName, tvUserPhone, tvUserPhoneStatus}, getActivity(), 1);

    }

    @Override
    public void openActivity(Context context, Class<?> cls) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void loadUserData(Response<UserProfileResponse> response) {
        UserProfileResponse.UserData userData = response.body().getUserData();
        tvUserName.setText(userData.getName());
        if (userData.getCity().size() > 0) {
            tvUserAddress.setText(userData.getCity().get(0).getCity_name());
        }
        if (getActivity() != null) {
            Glide.with((getActivity())).load(AppConstants.BASE_IMAGE_URL + userData.getImg()).into(imgUserProfile);
        }
        tvUserPhoneStatus.setText(userData.getMobile());
        tvUserMailStatus.setText(userData.getEmail());
    }
}
