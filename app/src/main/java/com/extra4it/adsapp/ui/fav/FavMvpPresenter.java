package com.extra4it.adsapp.ui.fav;

import android.widget.ProgressBar;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface FavMvpPresenter<V extends FavMvpView> extends MvpPresenter<V> {

    void loadUserFavData(ProgressBar progressBar);
}
