package com.extra4it.adsapp.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by user on 29/05/2018.
 */

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    private int currentPosition;

    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        setupFonts();
    }

    public void onBind(int position) {
        currentPosition = position;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    protected abstract void setupFonts();
}
