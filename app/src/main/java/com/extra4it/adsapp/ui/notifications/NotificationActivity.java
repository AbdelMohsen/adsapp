package com.extra4it.adsapp.ui.notifications;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends BaseActivity implements NotificationMvpView {
    // injection
    @Inject
    NotificationMvpPresenter<NotificationMvpView> presenter;
    @Inject
    LinearLayoutManager llManager;
    @Inject
    NotificationsAdapter notificationsAdapter;
    @BindView(R.id.tv_notification)
    TextView tvNotification;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.rv_notifications)
    RecyclerView rvNotifications;

    //widgets
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // change StatusBar color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // initialize clickListener
        onItemClicked();
        // initialize Fonts
        setupFonts();

        // initialize recyclerView
        rvNotifications.setLayoutManager(llManager);
        rvNotifications.setAdapter(notificationsAdapter);

    }

    @Override
    protected void onItemClicked() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvNotification}, this, 2);
    }
}
