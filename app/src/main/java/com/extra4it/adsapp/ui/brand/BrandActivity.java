package com.extra4it.adsapp.ui.brand;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.advertisment.AdvActivity;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.brand.citySheet.CityBottomSheetFragment;
import com.extra4it.adsapp.utils.Fonts;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class BrandActivity extends BaseActivity implements BrandMvpView, BrandAdapter.BrandClickCallBack, CityBottomSheetFragment.CityBottomSheetCallBack {
    // injection
    @Inject
    BrandMvpPresenter<BrandMvpView> presenter;
    @BindView(R.id.rv_brand)
    RecyclerView rvBrand;
    @BindView(R.id.img_location)
    ImageView imgLocation;
    @BindView(R.id.tv_brand_name)
    TextView tvBrandName;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.progress_bar)
    SpinKitView progressBar;
    private int subSectionId;
    private List<BrandResponse.Data> brandAdsList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand);
        ButterKnife.bind(this);

        // initialization
        init();
    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize itemClick
        onItemClicked();
        // initialize Fonts
        setupFonts();
        // change status color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // get intent Data
        if (getIntent() != null) {
            subSectionId = getIntent().getIntExtra("subSectionId", 0);

        }
        // initialize RecyclerView
        presenter.loadBrandAds(progressBar, subSectionId);

    }

    @Override
    protected void onItemClicked() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeActivity();
            }
        });

        imgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CityBottomSheetFragment cityBottomSheetFragment = new CityBottomSheetFragment();
                cityBottomSheetFragment.show(getSupportFragmentManager(), "city_bottom_sheet");
            }
        });
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvBrandName}, this, 2);
    }

    @Override
    public void onBrandClicked(int position) {
        Intent intent = new Intent(BrandActivity.this, AdvActivity.class);
        intent.putExtra("adId", brandAdsList.get(position).getId());
        Log.e("xxx", "" + brandAdsList.get(position).getId());
        startActivity(intent);
        overridePendingTransition(R.anim.push_up_out, R.anim.fade_out);
    }

    @Override
    public void onCityBottomSheetClick(int position, int cityId) {
        Log.e("xxx", "" + position);
        presenter.loadBrandAds(progressBar, subSectionId, cityId);
    }

    @Override
    public void showBrandAds(Response<BrandResponse> response) {
        brandAdsList = response.body().getData();
        rvBrand.setLayoutManager(new LinearLayoutManager(this));
        rvBrand.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(this, R.anim.item_anim));
        BrandAdapter brandAdapter = new BrandAdapter(this, brandAdsList);
        brandAdapter.setBrandCallBack(this);
        rvBrand.setAdapter(brandAdapter);
    }
}
