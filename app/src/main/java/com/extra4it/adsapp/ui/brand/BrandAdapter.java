package com.extra4it.adsapp.ui.brand;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.ui.base.BaseViewHolder;
import com.extra4it.adsapp.utils.AppConstants;
import com.extra4it.adsapp.utils.Fonts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BrandAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_NORMAL = 1;
    private Context context;
    private List<BrandResponse.Data> brandAdsList = new ArrayList<>();
    private LayoutInflater inflater;
    private BrandClickCallBack brandCallBack;

    public BrandAdapter(Context context, List<BrandResponse.Data> brandAdsList) {
        this.context = context;
        this.brandAdsList = brandAdsList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                View normalView = inflater.inflate(R.layout.row_brand, parent, false);
                return new BrandViewHolder(normalView);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(inflater.inflate(R.layout.row_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        if (brandAdsList != null && brandAdsList.size() > 0) {
            return brandAdsList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (brandAdsList != null && brandAdsList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }

    }

    public void setBrandCallBack(BrandClickCallBack brandCallBack) {
        this.brandCallBack = brandCallBack;
    }

    class EmptyViewHolder extends BaseViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void setupFonts() {

        }
    }

    class BrandViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.img_brand)
        ImageView imgBrand;
        @BindView(R.id.tv_ad_description)
        TextView tvAdDescription;
        @BindView(R.id.tv_user_name)
        TextView tvUserName;
        @BindView(R.id.tv_date)
        TextView tvDate;

        public BrandViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        protected void setupFonts() {
            Fonts.setFont(new TextView[]{tvDate, tvUserName}, context, 1);
            Fonts.setFont(new TextView[]{tvAdDescription}, context, 2);

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            Glide.with(context).load(AppConstants.BASE_IMAGE_URL + brandAdsList.get(position).getImg()).into(imgBrand);
            tvAdDescription.setText(brandAdsList.get(position).getTitle());
            tvUserName.setText(brandAdsList.get(position).getUser().get(0).getName());
            tvDate.setText(brandAdsList.get(position).getCreated_at());
        }

        @Override
        public void onClick(View view) {
            if (brandCallBack != null) {
                brandCallBack.onBrandClicked(getAdapterPosition());
            }
        }
    }

    public interface BrandClickCallBack {
        void onBrandClicked(int position);
    }
}
