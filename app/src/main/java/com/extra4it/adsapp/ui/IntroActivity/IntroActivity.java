package com.extra4it.adsapp.ui.IntroActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.login.LoginActivity;
import com.extra4it.adsapp.utils.Fonts;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IntroActivity extends BaseActivity implements IntroMvpView {
    private static final String TAG = "IntroActivity";
    @BindView(R.id.viewPagerSlider)
    ViewPager viewPagerSlider;
    @BindView(R.id.layoutDots)
    LinearLayout layoutDots;
    @BindView(R.id.tv_next)
    TextView btnNext;

    TextView[] dots;
    @BindView(R.id.intro_container)
    LinearLayout introContainer;
    @BindView(R.id.tv_skip)
    TextView tvSkip;
    private int layouts[];
    private int current = 0;

    @Inject
    IntroMvpPresenter<IntroMvpView> presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);

        init();


        addDots(0);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter("", "", "");
        viewPagerSlider.setAdapter(viewPagerAdapter);
        viewPagerSlider.setOnPageChangeListener(pageChangeListener);


    }

    @Override
    protected void init() {
        // initialize injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);

        // initialize fonts
        setupFonts();
        // initialize click listener
        onItemClicked();
        // change statusBarColor
        changeStatusBarColor(getResources().getColor(R.color.intro_background));
        layouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3
        };
    }

    @Override
    protected void onItemClicked() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNextClicked();
            }
        });
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onSkipClicked();
            }
        });
    }


    // setup Custom Fonts
    @Override
    public void setupFonts() {
        Fonts.setFont(new TextView[]{btnNext, tvSkip}, this, 1);
    }


    //    when user scroll the view pager
    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            current = position;
            addDots(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    /**
     * 1- initialize dotArray.Length wit layout.Array
     * 2- clear views from container layout
     * 3- loop dotArray and initialize each one of them as textView
     * 4- give each text view inActive color and same size
     * 5- give current text active color
     */
    private void addDots(int currentPage) {
        dots = new TextView[layouts.length];

        int colorActive = ContextCompat.getColor(IntroActivity.this, R.color.dot_active_color);
        int colorInactive = ContextCompat.getColor(this, R.color.colorGry);

        layoutDots.removeAllViews();

        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml(String.valueOf(fromHtml("•"))));
            dots[i].setTextColor(colorInactive);
            dots[i].setTextSize(45);
            layoutDots.addView(dots[i]);
        }
        dots[currentPage].setTextColor(colorActive);
        dots[currentPage].setBackground(getResources().getDrawable(R.drawable.dot_shape));

    }

    //    set dots to DotsLayout
    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    @Override
    public void slideIntro() {
        current = current + 1;
        if (current < layouts.length) {
            viewPagerSlider.setCurrentItem(current);
        } else {
            openLoginActivity();
        }
    }

    @Override
    public void openLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
    }


    /*
     * pager Adapter :
     * 1- extends pagerAdapter not fragmentPagerAdapter
     * 2- implement 4 methods
     * 2:1- instantiateItem : to inflate item and add view to container
     * 2:2- getCount : return the size of layout array
     * 2:3- isViewFromObject : return true if view page associated with specific key object from instantiateItem
     * 2:4- destroyItem : make container destroy current item
     * */
    public class ViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;
        TextView tvWelcome_1, tvWelcome_2, tvWelcome_3, tvWelcome_4, tvWelcome_5, tvWelcome_6;
        private String settings1, settings2, settings3;

        public ViewPagerAdapter(String settings1, String settings2, String settings3) {
            this.settings1 = settings1;
            this.settings2 = settings2;
            this.settings3 = settings3;

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = null;
            if (layoutInflater != null) {
                switch (position) {
                    case 0:
                        view = layoutInflater.inflate(layouts[0], container, false);
                        tvWelcome_1 = view.findViewById(R.id.tv1);
                        tvWelcome_2 = view.findViewById(R.id.tv2);

//                        tvWelcome_1.setText(settings1);
                        Fonts.setFont(new TextView[]{tvWelcome_1, tvWelcome_2}, IntroActivity.this, 1);
                        break;
                    case 1:
                        view = layoutInflater.inflate(layouts[1], container, false);
                        tvWelcome_3 = view.findViewById(R.id.tv3);
                        tvWelcome_4 = view.findViewById(R.id.tv4);
//                        tvWelcome_2.setText(settings2);
                        Fonts.setFont(new TextView[]{tvWelcome_3, tvWelcome_4}, IntroActivity.this, 1);
                        break;
                    case 2:
                        view = layoutInflater.inflate(layouts[2], container, false);
                        tvWelcome_5 = view.findViewById(R.id.tv5);
                        tvWelcome_6 = view.findViewById(R.id.tv6);

//                        tvWelcome_3.setText(settings3);
                        Fonts.setFont(new TextView[]{tvWelcome_5, tvWelcome_6}, IntroActivity.this, 1);
                        break;
                }


            }

            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {

            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}

