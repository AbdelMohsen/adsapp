package com.extra4it.adsapp.ui.conditions;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.ui.base.BasePresenter;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConditionsPresenter<V extends ConditionsMvpView> extends BasePresenter<V>
        implements ConditionsMvpPresenter<V> {


    @Inject
    public ConditionsPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void loadConditions() {
        getDataManager().getApiHelper().getConditions()
                .enqueue(new Callback<JSONObject>() {
                    @Override
                    public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                        JSONObject body = response.body();
                        try {
                            JSONObject data = body.getJSONObject("data");
                            String terms = data.getString("terms_of_use");
                            getMvpView().showConditions(terms);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<JSONObject> call, Throwable t) {

                    }
                });
    }
}
