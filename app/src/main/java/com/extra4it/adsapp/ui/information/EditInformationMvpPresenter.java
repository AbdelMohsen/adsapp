package com.extra4it.adsapp.ui.information;

import android.widget.EditText;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface EditInformationMvpPresenter<V extends EditInformationMvpView> extends MvpPresenter<V> {

    void onChangeUnFocuseBackground();

    void loadUserData();

    void updateUserInfo(EditText etUserName, EditText userMail);

}
