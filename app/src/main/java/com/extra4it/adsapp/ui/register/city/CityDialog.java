package com.extra4it.adsapp.ui.register.city;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.extra4it.adsapp.R;
import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.ui.base.BaseDialog;
import com.extra4it.adsapp.ui.brand.citySheet.CityAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

public class CityDialog extends BaseDialog implements CityMvpView, CityAdapter.CityCallBack {
    @Inject
    CityMvpPresenter<CityMvpView> presenter;
    @BindView(R.id.rv_city)
    RecyclerView rvCity;
    Unbinder unbinder;
    private List<CityResponse.Data> cityList;
    onCityCallBack onCityCallBack;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_city, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setup(View view) {
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        //
        presenter.loadCities();
    }

    @Override
    protected void setupFont() {

    }

    @Override
    protected void onItemClick() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onCityCallBack) {
            onCityCallBack = (CityDialog.onCityCallBack) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showCities(Response<CityResponse> response) {
        rvCity.setLayoutManager(new LinearLayoutManager(getActivity()));
        cityList = response.body().getData();
        CityAdapter cityAdapter = new CityAdapter(getActivity(), cityList);
        cityAdapter.setCityCallBack(this);
        rvCity.setAdapter(cityAdapter);
    }

    @Override
    public void onCityClick(int position) {
        onCityCallBack.onCityClicked(cityList.get(position).getCity_name(), cityList.get(position).getId());
        dismiss();
    }

    public interface onCityCallBack {
        void onCityClicked(String cityName, int cityId);
    }
}
