package com.extra4it.adsapp.ui.confirmPay;

import com.extra4it.adsapp.ui.base.MvpPresenter;

public interface ConfirmPayMvpPresenter<V extends ConfirmPayMvpView> extends MvpPresenter<V> {
}
