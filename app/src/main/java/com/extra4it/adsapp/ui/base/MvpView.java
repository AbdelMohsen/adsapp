package com.extra4it.adsapp.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import java.util.Map;

/**
 * Created by user on 16/05/2018.
 */

public interface MvpView {

    void showLoading(ProgressBar progressBar);

    void hideLoading(ProgressBar progressBar);

    void showMessage(View view, String message);

    void hideKeyboard();

    boolean inNetworkConnected();

    void showSnackBar(View view, String message);

    void openActivity(Context context, Class<?> cls);

}
