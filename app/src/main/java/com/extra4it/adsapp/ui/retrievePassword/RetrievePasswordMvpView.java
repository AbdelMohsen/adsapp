package com.extra4it.adsapp.ui.retrievePassword;

import com.extra4it.adsapp.ui.base.ActivityMvpView;

public interface RetrievePasswordMvpView extends ActivityMvpView {
    void confirmPhone();

    void confirmCode();

    void confirmPassword();

    void openMainActivity();
}
