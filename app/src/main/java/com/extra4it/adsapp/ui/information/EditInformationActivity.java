package com.extra4it.adsapp.ui.information;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.extra4it.adsapp.R;
import com.extra4it.adsapp.ui.base.BaseActivity;
import com.extra4it.adsapp.ui.base.BasePresenter;
import com.extra4it.adsapp.utils.AppConstants;
import com.extra4it.adsapp.utils.Fonts;
import com.extra4it.adsapp.utils.PickImage;
import com.extra4it.adsapp.utils.RunTimePermission;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.extra4it.adsapp.utils.AppConstants.CAMERA_REQUEST_CODE;
import static com.extra4it.adsapp.utils.AppConstants.IMAGE_REQUEST_CODE;

public class EditInformationActivity extends BaseActivity implements EditInformationMvpView {
    @Inject
    EditInformationMvpPresenter<EditInformationMvpView> presenter;
    @Inject
    RunTimePermission permission;
    @Inject
    PickImage pickImage;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.tv_edit_data)
    TextView tvEditData;
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.img_avatar)
    CircleImageView imgAvatar;
    @BindView(R.id.et_user_name)
    EditText etUserName;
    @BindView(R.id.tl_user_name)
    TextInputLayout tlUserName;
    @BindView(R.id.et_user_mail)
    EditText etUserMail;
    @BindView(R.id.tl_user_mail)
    TextInputLayout tlUserMail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_information);
        ButterKnife.bind(this);

        // initialization
        init();

    }

    @Override
    protected void init() {
        // injection
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        // initialize clickListener
        onItemClicked();
        // initialize fonts
        setupFonts();
        // change statusBar color
        changeStatusBarColor(getResources().getColor(R.color.colorWhite));
        // load user data
        presenter.loadUserData();
    }

    @Override
    protected void onItemClicked() {
        etUserName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                presenter.onChangeUnFocuseBackground();
                tlUserName.setBackground(getResources().getDrawable(R.drawable.solid_white_shape));
                return false;
            }
        });

        etUserMail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                presenter.onChangeUnFocuseBackground();
                tlUserMail.setBackground(getResources().getDrawable(R.drawable.solid_white_shape));
                return false;
            }
        });
        imgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                permission.selectImage();
            }
        });

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.updateUserInfo(etUserName,etUserMail);
            }
        });
    }

    @Override
    public void releaseBackGround() {
        tlUserName.setBackground(getResources().getDrawable(R.drawable.transparent_shape));
        tlUserMail.setBackground(getResources().getDrawable(R.drawable.transparent_shape));
    }

    @Override
    public void loadUSerInformation(String imageUrl, String userName, String userMail) {
        Glide.with(this).load(AppConstants.BASE_IMAGE_URL + imageUrl).into(imgAvatar);
        etUserName.setText(userName);
        etUserMail.setText(userMail);
    }

    @Override
    public void loadChooseImage(Bitmap bitmap) {
        imgAvatar.setImageBitmap(bitmap);
    }

    @Override
    protected void setupFonts() {
        Fonts.setFont(new TextView[]{tvConfirm, tvEditData}, this, 1);
        Fonts.setFont(new TextInputLayout[]{tlUserMail, tlUserName}, this, 1);
        Fonts.setFont(new EditText[]{etUserMail, etUserName}, this, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == IMAGE_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    pickImage.openGallery();
                }
            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {

            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    pickImage.openCamera();
                }

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        pickImage.onActivityResult((BasePresenter) presenter, requestCode, resultCode, data);
    }
}
