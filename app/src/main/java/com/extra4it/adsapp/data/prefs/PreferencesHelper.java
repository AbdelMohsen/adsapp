package com.extra4it.adsapp.data.prefs;


import com.extra4it.adsapp.data.DataManager;

/**
 * Created by user on 07/05/2018.
 */

public interface PreferencesHelper {

    int getCurrentUserLoggedInMode();

    void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode);

    int getCurrentUserId();

    void setCurrentUserId(int userId);

    String getCurrentUserName();

    void setCurrentUserName(String userName);

    String getCurrentUserEmail();

    void setCurrentUserEmail(String email);

    String getCurrentUserProfilePicUrl();

    void setCurrentUserProfilePicURL(String profilePicUrl);

    String getAccessToken();

    void SetAccessToken(String accessToken);

    void setRememberUser(boolean isRemember);

    boolean getRememberUser();

    void setCurrentUserPhoneNum(String phoneNum);

    String getCurrentUserPhoneNum();
}
