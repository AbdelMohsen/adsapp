package com.extra4it.adsapp.data;


import com.extra4it.adsapp.data.network.ApiHelper;
import com.extra4it.adsapp.data.prefs.PreferencesHelper;

/**
 * Created by Abdel-Mohsen on 07/05/2018.
 */

public interface DataManager extends PreferencesHelper {
    // when user Is Logout
    void setUserAsLoggedOut();

    // act as constructor take all user info
    void UpdateUserInfo(String accessToken,
                        int userId,
                        LoggedInMode loggedInMode,
                        String userEmail,
                        String userName,
                        String profilePicURl,
                        String phoneNum);

    /**
     * enum class that know user login mode
     * in Data Manager : implemented by AppDataManager
     * and AppDataManager Contact With Whole App
     */
    enum LoggedInMode {
        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_GOOGLE(1),
        LOGGED_IN_MODE_FB(2),
        LOGGED_IN_MODE_SERVER(3);

        private final int type;

        LoggedInMode(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }

    ApiHelper getApiHelper();
}
