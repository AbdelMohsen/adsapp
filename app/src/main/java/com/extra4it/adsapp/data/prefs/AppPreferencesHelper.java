package com.extra4it.adsapp.data.prefs;


import android.content.Context;
import android.content.SharedPreferences;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.di.ApplicationContext;
import com.extra4it.adsapp.di.PreferencesInfo;
import com.extra4it.adsapp.utils.AppConstants;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by user on 07/05/2018.
 */
@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    // constant String Keys for SharedPreferences

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";
    private static final String PREF_KEY_CURRENT_USER_PROFILE_PIC_URL
            = "PREF_KEY_CURRENT_USER_PROFILE_PIC_URL";
    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";

    private static final String PREF_KEY_REMEMBER_ME = "PREF_KEY_REMEMBER_ME";
    private static final String PREF_KEY_CURRENT_PHONE_NUM = "PREF_KEY_CURRENT_PHONE_NUM";


    private final SharedPreferences mPref;

    /* @ApplicationContext,PreferencesInfo :
     * dependencies will be provided into Application module
     **/
    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferencesInfo String prefFileName) {

        mPref = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPref.getInt(PREF_KEY_USER_LOGGED_IN_MODE,
                DataManager.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.getType());
    }

    @Override
    public void setCurrentUserLoggedInMode(DataManager.LoggedInMode mode) {
        mPref.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.getType()).apply();
    }

    @Override
    public int getCurrentUserId() {
        int userId = mPref.getInt(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX);
        return userId == -1 ? AppConstants.NULL_INDEX : userId;
    }

    @Override
    public void setCurrentUserId(int userId) {
        int id = userId == -1 ? AppConstants.NULL_INDEX : userId;
        mPref.edit().putInt(PREF_KEY_CURRENT_USER_ID, id).apply();
    }

    @Override
    public String getCurrentUserName() {
        return mPref.getString(PREF_KEY_CURRENT_USER_NAME, null);
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPref.edit().putString(PREF_KEY_CURRENT_USER_NAME, userName).apply();
    }

    @Override
    public String getCurrentUserEmail() {
        return mPref.getString(PREF_KEY_CURRENT_USER_EMAIL, null);
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPref.edit().putString(PREF_KEY_CURRENT_USER_EMAIL, email).apply();
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPref.getString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, null);

    }

    @Override
    public void setCurrentUserProfilePicURL(String profilePicUrl) {
        mPref.edit().putString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, profilePicUrl).apply();
    }

    @Override
    public String getAccessToken() {
        return mPref.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void SetAccessToken(String accessToken) {
        mPref.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public void setRememberUser(boolean isRemember) {
        mPref.edit().putBoolean(PREF_KEY_REMEMBER_ME, isRemember).apply();
    }

    @Override
    public boolean getRememberUser() {
        return mPref.getBoolean(PREF_KEY_REMEMBER_ME, false);
    }

    @Override
    public void setCurrentUserPhoneNum(String phoneNum) {
        mPref.edit().putString(PREF_KEY_CURRENT_PHONE_NUM, phoneNum).apply();
    }

    @Override
    public String getCurrentUserPhoneNum() {
        return mPref.getString(PREF_KEY_CURRENT_PHONE_NUM, null);
    }
}
