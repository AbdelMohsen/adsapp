package com.extra4it.adsapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BrandResponse {


    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("countData")
    private int countData;
    @Expose
    @SerializedName("status")
    private boolean status;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public int getCountData() {
        return countData;
    }

    public void setCountData(int countData) {
        this.countData = countData;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("sub_section")
        private List<Sub_section> sub_section;
        @Expose
        @SerializedName("section")
        private List<Section> section;
        @Expose
        @SerializedName("user")
        private List<User> user;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("img")
        private String img;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("sub_cat_id")
        private int sub_cat_id;
        @Expose
        @SerializedName("section_id")
        private int section_id;
        @Expose
        @SerializedName("longt")
        private String longt;
        @Expose
        @SerializedName("lat")
        private String lat;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("city_id")
        private int city_id;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("id")
        private int id;

        public List<Sub_section> getSub_section() {
            return sub_section;
        }

        public void setSub_section(List<Sub_section> sub_section) {
            this.sub_section = sub_section;
        }

        public List<Section> getSection() {
            return section;
        }

        public void setSection(List<Section> section) {
            this.section = section;
        }

        public List<User> getUser() {
            return user;
        }

        public void setUser(List<User> user) {
            this.user = user;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getSub_cat_id() {
            return sub_cat_id;
        }

        public void setSub_cat_id(int sub_cat_id) {
            this.sub_cat_id = sub_cat_id;
        }

        public int getSection_id() {
            return section_id;
        }

        public void setSection_id(int section_id) {
            this.section_id = section_id;
        }

        public String getLongt() {
            return longt;
        }

        public void setLongt(String longt) {
            this.longt = longt;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Sub_section {
        @Expose
        @SerializedName("sub_img")
        private String sub_img;
        @Expose
        @SerializedName("sub_name")
        private String sub_name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getSub_img() {
            return sub_img;
        }

        public void setSub_img(String sub_img) {
            this.sub_img = sub_img;
        }

        public String getSub_name() {
            return sub_name;
        }

        public void setSub_name(String sub_name) {
            this.sub_name = sub_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Section {
        @Expose
        @SerializedName("cat_img")
        private String cat_img;
        @Expose
        @SerializedName("cat_name")
        private String cat_name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getCat_img() {
            return cat_img;
        }

        public void setCat_img(String cat_img) {
            this.cat_img = cat_img;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class User {
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
