package com.extra4it.adsapp.data.network;

import com.extra4it.adsapp.data.network.model.BankAccountResponse;
import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.data.network.model.CityResponse;
import com.extra4it.adsapp.data.network.model.DefaultResponse;
import com.extra4it.adsapp.data.network.model.HomeTagsResponse;
import com.extra4it.adsapp.data.network.model.LoginResponse;
import com.extra4it.adsapp.data.network.model.RegisterResponse;
import com.extra4it.adsapp.data.network.model.SectionResponse;
import com.extra4it.adsapp.data.network.model.SelectedAdResponse;
import com.extra4it.adsapp.data.network.model.UpdateResponse;
import com.extra4it.adsapp.data.network.model.UserProfileResponse;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by user on 15/05/2018.
 */

public interface ApiHelper {
    @Multipart
    @POST("register")
    Call<RegisterResponse> register(@Part("name") RequestBody userName,
                                    @Part("email") RequestBody userMail,
                                    @Part("password") RequestBody password,
                                    @Part("mobile") RequestBody phoneNum,
                                    @Part("city_id") int cityId,
                                    @Part MultipartBody.Part file);

    @POST("login")
    @FormUrlEncoded
    Call<LoginResponse> login(@Field("name") String userName,
                              @Field("password") String password);

    @GET("sections")
    Call<HomeTagsResponse> getTags();

    @POST("sub_sections")
    @FormUrlEncoded
    Call<SectionResponse> getSubSections(@Field("section_id") int sectionId);

    @GET("getSetting")
    Call<JSONObject> getConditions();

    @POST("ads_in_section")
    @FormUrlEncoded
    Call<BrandResponse> getBrandAds(@Field("sub_cat_id") int subSectionId);

    @GET("cities")
    Call<CityResponse> getCities();

    @POST("ads_in_section")
    @FormUrlEncoded
    Call<BrandResponse> getBrandAds(@Field("sub_cat_id") int subSectionId,
                                    @Field("city_id") int cityId);

    @POST("ad_data")
    @FormUrlEncoded
    Call<SelectedAdResponse> getAdData(@Header("Authorization") String token,
                                       @Field("ad_id") int adId);

    @POST("add_rating")
    @FormUrlEncoded
    Call<DefaultResponse> addRate(@Header("Authorization") String token,
                                  @Field("ad_id") int adId,
                                  @Field("rate") int rate);

    @POST("add_ad_fav")
    @FormUrlEncoded
    Call<DefaultResponse> addFav(@Header("Authorization") String token,
                                 @Field("ad_id") int adId);

    @POST("add_like")
    @FormUrlEncoded
    Call<DefaultResponse> addLike(@Header("Authorization") String token,
                                  @Field("ad_id") int adId);

    @POST("add_comment")
    @FormUrlEncoded
    Call<DefaultResponse> addComment(@Header("Authorization") String token,
                                     @Field("ad_id") int adId,
                                     @Field("comment") String comment);

    @POST("add_report")
    @FormUrlEncoded
    Call<DefaultResponse> addReport(@Header("Authorization") String token,
                                    @Field("ad_id") int adId,
                                    @Field("report") String reportText);

    @GET("getUser")
    Call<UserProfileResponse> getUserData(@Header("Authorization") String token);

    @POST("contact")
    @FormUrlEncoded
    Call<DefaultResponse> contact(@Header("Authorization") String token,
                                  @Field("msg") String msg);

    @Multipart
    @POST("editUser")
    Call<UpdateResponse> updateUserData(@Header("Authorization") String token,
                                        @Part("name") RequestBody userName,
                                        @Part("email") RequestBody userMail,
                                        @Part MultipartBody.Part file);

    @POST("change_phone")
    @FormUrlEncoded
    Call<DefaultResponse> changePhoneNum(@Header("Authorization") String token,
                                         @Field("old") String oldPhone,
                                         @Field("password") String password,
                                         @Field("new") String newPhone);

    @POST("change_password")
    @FormUrlEncoded
    Call<DefaultResponse> changePassword(@Header("Authorization") String token,
                                         @Field("old") String oldPassword,
                                         @Field("new") String newPassword);

    @GET("getBanks")
    Call<BankAccountResponse> getBankAccounts();

    @GET("user_ad")
    Call<BrandResponse> getUserAds(@Header("Authorization") String token);

    @GET("my_favorites")
    Call<BrandResponse> getUserFavAds(@Header("Authorization") String token);
}
