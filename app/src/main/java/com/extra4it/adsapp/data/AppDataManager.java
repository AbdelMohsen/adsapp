package com.extra4it.adsapp.data;


import android.content.Context;

import com.extra4it.adsapp.data.network.ApiHelper;
import com.extra4it.adsapp.data.prefs.PreferencesHelper;
import com.extra4it.adsapp.di.ApplicationContext;
import com.extra4it.adsapp.utils.AppConstants;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by user on 07/05/2018.
 */

@Singleton
public class AppDataManager implements DataManager {

    private final Context mContext;
    private final PreferencesHelper mPreferencesHelper;
    private final ApiHelper apiHelper;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        this.mContext = context;
        this.mPreferencesHelper = preferencesHelper;
        this.apiHelper = apiHelper;
    }

    @Override
    public int getCurrentUserLoggedInMode() {
        return mPreferencesHelper.getCurrentUserLoggedInMode();
    }

    @Override
    public void setCurrentUserLoggedInMode(LoggedInMode mode) {
        mPreferencesHelper.setCurrentUserLoggedInMode(mode);
    }

    @Override
    public int getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(int userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public String getCurrentUserName() {
        return mPreferencesHelper.getCurrentUserName();
    }

    @Override
    public void setCurrentUserName(String userName) {
        mPreferencesHelper.setCurrentUserName(userName);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicURL(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicURL(profilePicUrl);
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void SetAccessToken(String accessToken) {
        mPreferencesHelper.SetAccessToken(accessToken);
    }

    @Override
    public void setRememberUser(boolean isRemember) {
        mPreferencesHelper.setRememberUser(isRemember);
    }

    @Override
    public boolean getRememberUser() {
        return mPreferencesHelper.getRememberUser();
    }

    @Override
    public void setCurrentUserPhoneNum(String phoneNum) {
        mPreferencesHelper.setCurrentUserPhoneNum(phoneNum);
    }

    @Override
    public String getCurrentUserPhoneNum() {
        return mPreferencesHelper.getCurrentUserPhoneNum();
    }

    @Override
    public void setUserAsLoggedOut() {
        UpdateUserInfo(null,
                AppConstants.NULL_INDEX,
                LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT,
                null,
                null,
                null,
                null);
    }

    @Override
    public void UpdateUserInfo(String accessToken, int userId, LoggedInMode loggedInMode, String userEmail, String userName, String profilePicURl, String phoneNum) {
        SetAccessToken(accessToken);
        setCurrentUserId(userId);
        setCurrentUserLoggedInMode(loggedInMode);
        setCurrentUserEmail(userEmail);
        setCurrentUserName(userName);
        setCurrentUserProfilePicURL(profilePicURl);
        setCurrentUserPhoneNum(phoneNum);
    }

    @Override
    public ApiHelper getApiHelper() {
        return apiHelper;
    }

}
