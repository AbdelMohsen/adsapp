package com.extra4it.adsapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SelectedAdResponse {


    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("avgRating")
        private int avgRating;
        @Expose
        @SerializedName("userRating")
        private int userRating;
        @Expose
        @SerializedName("userfavorite")
        private int userfavorite;
        @Expose
        @SerializedName("liksCount")
        private int liksCount;
        @Expose
        @SerializedName("liks")
        private List<Liks> liks;
        @Expose
        @SerializedName("commentsCount")
        private int commentsCount;
        @Expose
        @SerializedName("comments")
        private List<Comments> comments;
        @Expose
        @SerializedName("ad")
        private Ad ad;

        public int getAvgRating() {
            return avgRating;
        }

        public void setAvgRating(int avgRating) {
            this.avgRating = avgRating;
        }

        public int getUserRating() {
            return userRating;
        }

        public void setUserRating(int userRating) {
            this.userRating = userRating;
        }

        public int getUserfavorite() {
            return userfavorite;
        }

        public void setUserfavorite(int userfavorite) {
            this.userfavorite = userfavorite;
        }

        public int getLiksCount() {
            return liksCount;
        }

        public void setLiksCount(int liksCount) {
            this.liksCount = liksCount;
        }

        public List<Liks> getLiks() {
            return liks;
        }

        public void setLiks(List<Liks> liks) {
            this.liks = liks;
        }

        public int getCommentsCount() {
            return commentsCount;
        }

        public void setCommentsCount(int commentsCount) {
            this.commentsCount = commentsCount;
        }

        public List<Comments> getComments() {
            return comments;
        }

        public void setComments(List<Comments> comments) {
            this.comments = comments;
        }

        public Ad getAd() {
            return ad;
        }

        public void setAd(Ad ad) {
            this.ad = ad;
        }
    }

    public static class Liks {
        @Expose
        @SerializedName("users2")
        private List<Users2> users2;
        @Expose
        @SerializedName("ad_id")
        private int ad_id;
        @Expose
        @SerializedName("like_in")
        private String like_in;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("id")
        private int id;

        public List<Users2> getUsers2() {
            return users2;
        }

        public void setUsers2(List<Users2> users2) {
            this.users2 = users2;
        }

        public int getAd_id() {
            return ad_id;
        }

        public void setAd_id(int ad_id) {
            this.ad_id = ad_id;
        }

        public String getLike_in() {
            return like_in;
        }

        public void setLike_in(String like_in) {
            this.like_in = like_in;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Users2 {
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Comments {
        @Expose
        @SerializedName("users")
        private List<Users> users;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("ad_id")
        private int ad_id;
        @Expose
        @SerializedName("comment")
        private String comment;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("id")
        private int id;

        public List<Users> getUsers() {
            return users;
        }

        public void setUsers(List<Users> users) {
            this.users = users;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getAd_id() {
            return ad_id;
        }

        public void setAd_id(int ad_id) {
            this.ad_id = ad_id;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Users {
        @Expose
        @SerializedName("img")
        private String img;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Ad {
        @Expose
        @SerializedName("city")
        private List<City> city;
        @Expose
        @SerializedName("sub_section")
        private List<Sub_section> sub_section;
        @Expose
        @SerializedName("user")
        private List<User> user;
        @Expose
        @SerializedName("section")
        private List<Section> section;
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("active")
        private int active;
        @Expose
        @SerializedName("img3")
        private String img3;
        @Expose
        @SerializedName("img2")
        private String img2;
        @Expose
        @SerializedName("img1")
        private String img1;
        @Expose
        @SerializedName("img")
        private String img;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("phone")
        private String phone;
        @Expose
        @SerializedName("sub_cat_id")
        private int sub_cat_id;
        @Expose
        @SerializedName("section_id")
        private int section_id;
        @Expose
        @SerializedName("longt")
        private String longt;
        @Expose
        @SerializedName("lat")
        private String lat;
        @Expose
        @SerializedName("address")
        private String address;
        @Expose
        @SerializedName("city_id")
        private int city_id;
        @Expose
        @SerializedName("content")
        private String content;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("id")
        private int id;

        public List<City> getCity() {
            return city;
        }

        public void setCity(List<City> city) {
            this.city = city;
        }

        public List<Sub_section> getSub_section() {
            return sub_section;
        }

        public void setSub_section(List<Sub_section> sub_section) {
            this.sub_section = sub_section;
        }

        public List<User> getUser() {
            return user;
        }

        public void setUser(List<User> user) {
            this.user = user;
        }

        public List<Section> getSection() {
            return section;
        }

        public void setSection(List<Section> section) {
            this.section = section;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
        }

        public String getImg3() {
            return img3;
        }

        public void setImg3(String img3) {
            this.img3 = img3;
        }

        public String getImg2() {
            return img2;
        }

        public void setImg2(String img2) {
            this.img2 = img2;
        }

        public String getImg1() {
            return img1;
        }

        public void setImg1(String img1) {
            this.img1 = img1;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getSub_cat_id() {
            return sub_cat_id;
        }

        public void setSub_cat_id(int sub_cat_id) {
            this.sub_cat_id = sub_cat_id;
        }

        public int getSection_id() {
            return section_id;
        }

        public void setSection_id(int section_id) {
            this.section_id = section_id;
        }

        public String getLongt() {
            return longt;
        }

        public void setLongt(String longt) {
            this.longt = longt;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class City {
        @Expose
        @SerializedName("city_name")
        private String city_name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Sub_section {
        @Expose
        @SerializedName("sub_img")
        private String sub_img;
        @Expose
        @SerializedName("sub_name")
        private String sub_name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getSub_img() {
            return sub_img;
        }

        public void setSub_img(String sub_img) {
            this.sub_img = sub_img;
        }

        public String getSub_name() {
            return sub_name;
        }

        public void setSub_name(String sub_name) {
            this.sub_name = sub_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class User {
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Section {
        @Expose
        @SerializedName("cat_img")
        private String cat_img;
        @Expose
        @SerializedName("cat_name")
        private String cat_name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getCat_img() {
            return cat_img;
        }

        public void setCat_img(String cat_img) {
            this.cat_img = cat_img;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
