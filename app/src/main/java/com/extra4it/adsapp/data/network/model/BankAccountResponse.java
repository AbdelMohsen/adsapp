package com.extra4it.adsapp.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BankAccountResponse {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("banks")
        private List<Banks> banks;
        @Expose
        @SerializedName("text")
        private String text;

        public List<Banks> getBanks() {
            return banks;
        }

        public void setBanks(List<Banks> banks) {
            this.banks = banks;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    public static class Banks {
        @Expose
        @SerializedName("iban")
        private String iban;
        @Expose
        @SerializedName("active")
        private int active;
        @Expose
        @SerializedName("acc_num")
        private String acc_num;
        @Expose
        @SerializedName("acc_name")
        private String acc_name;
        @Expose
        @SerializedName("bank_name")
        private String bank_name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getIban() {
            return iban;
        }

        public void setIban(String iban) {
            this.iban = iban;
        }

        public int getActive() {
            return active;
        }

        public void setActive(int active) {
            this.active = active;
        }

        public String getAcc_num() {
            return acc_num;
        }

        public void setAcc_num(String acc_num) {
            this.acc_num = acc_num;
        }

        public String getAcc_name() {
            return acc_name;
        }

        public void setAcc_name(String acc_name) {
            this.acc_name = acc_name;
        }

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
