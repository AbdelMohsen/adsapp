package com.extra4it.adsapp;


import android.app.Application;

import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.di.component.ApplicationComponent;
import com.extra4it.adsapp.di.component.DaggerApplicationComponent;
import com.extra4it.adsapp.di.module.ApplicationModule;
import com.extra4it.adsapp.utils.AppConstants;

import javax.inject.Inject;

public class AdsApp extends Application {

    @Inject
    DataManager mDataManager;

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.
                builder()
                .applicationModule(new ApplicationModule(this, AppConstants.BASE_URL))
                .build();
        applicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
