package com.extra4it.adsapp.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by user on 07/05/2018.
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface PreferencesInfo {
}
