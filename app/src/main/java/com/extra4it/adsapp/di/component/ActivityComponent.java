package com.extra4it.adsapp.di.component;


import com.extra4it.adsapp.di.PerActivity;
import com.extra4it.adsapp.di.module.ActivityModule;
import com.extra4it.adsapp.ui.ForbiddenGoods.ForbiddenActivity;
import com.extra4it.adsapp.ui.IntroActivity.IntroActivity;
import com.extra4it.adsapp.ui.add.AddFragment;
import com.extra4it.adsapp.ui.advertisment.AdvActivity;
import com.extra4it.adsapp.ui.bankAccount.BankAccountActivity;
import com.extra4it.adsapp.ui.brand.BrandActivity;
import com.extra4it.adsapp.ui.brand.citySheet.CityBottomSheetFragment;
import com.extra4it.adsapp.ui.changePassword.ChangePasswordActivity;
import com.extra4it.adsapp.ui.changePhone.ChangePhoneActivity;
import com.extra4it.adsapp.ui.conditions.ConditionsActivity;
import com.extra4it.adsapp.ui.confirmPay.ConfirmPayActivity;
import com.extra4it.adsapp.ui.contact.ContactActivity;
import com.extra4it.adsapp.ui.fav.FavFragment;
import com.extra4it.adsapp.ui.home.HomeFragment;
import com.extra4it.adsapp.ui.information.EditInformationActivity;
import com.extra4it.adsapp.ui.likes.LikesActivity;
import com.extra4it.adsapp.ui.login.LoginActivity;
import com.extra4it.adsapp.ui.main.MainActivity;
import com.extra4it.adsapp.ui.myAds.MyAdsActivity;
import com.extra4it.adsapp.ui.notifications.NotificationActivity;
import com.extra4it.adsapp.ui.profile.ProfileFragment;
import com.extra4it.adsapp.ui.rating.RatingDialog;
import com.extra4it.adsapp.ui.recharge.RechargeActivity;
import com.extra4it.adsapp.ui.register.RegisterActivity;
import com.extra4it.adsapp.ui.register.city.CityDialog;
import com.extra4it.adsapp.ui.report.ReportActivity;
import com.extra4it.adsapp.ui.retrievePassword.RetrievePasswordActivity;
import com.extra4it.adsapp.ui.search.SearchFragment;
import com.extra4it.adsapp.ui.searchResult.SearchResultActivity;
import com.extra4it.adsapp.ui.setting.SettingActivity;
import com.extra4it.adsapp.ui.splash.SplashActivity;
import com.extra4it.adsapp.ui.user.UserActivity;

import dagger.Component;

/**
 * Created by user on 09/05/2018.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(SplashActivity splashActivity);

    void inject(IntroActivity introActivity);

    void inject(LoginActivity loginActivity);

    void inject(RegisterActivity registerActivity);

    void inject(RetrievePasswordActivity retrievePasswordActivity);

    void inject(MainActivity mainActivity);

    void inject(HomeFragment homeFragment);

    void inject(BrandActivity brandActivity);

    void inject(CityBottomSheetFragment cityBottomSheet);

    void inject(FavFragment favFragment);

    void inject(BankAccountActivity bankAccountActivity);

    void inject(ForbiddenActivity forbiddenActivity);

    void inject(ConditionsActivity conditionsActivity);

    void inject(AdvActivity advActivity);

    void inject(ReportActivity reportActivity);

    void inject(RatingDialog ratingDialog);

    void inject(LikesActivity likesActivity);

    void inject(SearchFragment searchFragment);

    void inject(SearchResultActivity searchResultActivity);

    void inject(ContactActivity contactActivity);

    void inject(ProfileFragment profileFragment);

    void inject(MyAdsActivity myAdsActivity);

    void inject(SettingActivity settingActivity);

    void inject(EditInformationActivity editInformationActivity);

    void inject(ChangePhoneActivity changePhoneActivity);

    void inject(ChangePasswordActivity changePasswordActivity);

    void inject(RechargeActivity rechargeActivity);

    void inject(ConfirmPayActivity confirmPayActivity);

    void inject(NotificationActivity notificationActivity);

    void inject(UserActivity userActivity);

    void inject(CityDialog cityDialog);

    void inject(AddFragment addFragment);



}




