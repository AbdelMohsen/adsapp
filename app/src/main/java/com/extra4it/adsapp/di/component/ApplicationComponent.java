package com.extra4it.adsapp.di.component;


import android.app.Application;
import android.content.Context;

import com.extra4it.adsapp.AdsApp;
import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.di.ApplicationContext;
import com.extra4it.adsapp.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by user on 09/05/2018.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(AdsApp adsApp);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();

}
