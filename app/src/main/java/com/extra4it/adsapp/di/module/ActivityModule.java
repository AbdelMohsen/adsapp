package com.extra4it.adsapp.di.module;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.extra4it.adsapp.data.network.model.BrandResponse;
import com.extra4it.adsapp.data.network.model.NotificationsResponse;
import com.extra4it.adsapp.di.ActivityContext;
import com.extra4it.adsapp.ui.ForbiddenGoods.ForbiddenMvpPresenter;
import com.extra4it.adsapp.ui.ForbiddenGoods.ForbiddenMvpView;
import com.extra4it.adsapp.ui.ForbiddenGoods.ForbiddenPresenter;
import com.extra4it.adsapp.ui.IntroActivity.IntroMvpPresenter;
import com.extra4it.adsapp.ui.IntroActivity.IntroMvpView;
import com.extra4it.adsapp.ui.IntroActivity.IntroPresenter;
import com.extra4it.adsapp.ui.add.AddMvpPresenter;
import com.extra4it.adsapp.ui.add.AddMvpView;
import com.extra4it.adsapp.ui.add.AddPresenter;
import com.extra4it.adsapp.ui.advertisment.AdvMvpPresenter;
import com.extra4it.adsapp.ui.advertisment.AdvMvpView;
import com.extra4it.adsapp.ui.advertisment.AdvPresenter;
import com.extra4it.adsapp.ui.bankAccount.BankAccountMvpPresenter;
import com.extra4it.adsapp.ui.bankAccount.BankAccountMvpView;
import com.extra4it.adsapp.ui.bankAccount.BankAccountPresenter;
import com.extra4it.adsapp.ui.brand.BrandMvpPresenter;
import com.extra4it.adsapp.ui.brand.BrandMvpView;
import com.extra4it.adsapp.ui.brand.BrandPresenter;
import com.extra4it.adsapp.ui.brand.citySheet.CitySheetMvpPresenter;
import com.extra4it.adsapp.ui.brand.citySheet.CitySheetMvpView;
import com.extra4it.adsapp.ui.brand.citySheet.CitySheetPresenter;
import com.extra4it.adsapp.ui.changePassword.ChangePasswordMvpPresenter;
import com.extra4it.adsapp.ui.changePassword.ChangePasswordMvpView;
import com.extra4it.adsapp.ui.changePassword.ChangePasswordPresenter;
import com.extra4it.adsapp.ui.changePhone.ChangePhoneMvpPresenter;
import com.extra4it.adsapp.ui.changePhone.ChangePhoneMvpView;
import com.extra4it.adsapp.ui.changePhone.ChangePhonePresenter;
import com.extra4it.adsapp.ui.conditions.ConditionsMvpPresenter;
import com.extra4it.adsapp.ui.conditions.ConditionsMvpView;
import com.extra4it.adsapp.ui.conditions.ConditionsPresenter;
import com.extra4it.adsapp.ui.confirmPay.ConfirmPayMvpPresenter;
import com.extra4it.adsapp.ui.confirmPay.ConfirmPayMvpView;
import com.extra4it.adsapp.ui.confirmPay.ConfirmPayPresenter;
import com.extra4it.adsapp.ui.contact.ContactMvpPresenter;
import com.extra4it.adsapp.ui.contact.ContactMvpView;
import com.extra4it.adsapp.ui.contact.ContactPresenter;
import com.extra4it.adsapp.ui.fav.FavMvpPresenter;
import com.extra4it.adsapp.ui.fav.FavMvpView;
import com.extra4it.adsapp.ui.fav.FavPresenter;
import com.extra4it.adsapp.ui.home.HomeMvpPresenter;
import com.extra4it.adsapp.ui.home.HomeMvpView;
import com.extra4it.adsapp.ui.home.HomePresenter;
import com.extra4it.adsapp.ui.information.EditInformationMvpPresenter;
import com.extra4it.adsapp.ui.information.EditInformationMvpView;
import com.extra4it.adsapp.ui.information.EditInformationPresenter;
import com.extra4it.adsapp.ui.likes.LikesMvpPresenter;
import com.extra4it.adsapp.ui.likes.LikesMvpView;
import com.extra4it.adsapp.ui.likes.LikesPresenter;
import com.extra4it.adsapp.ui.login.LoginMvpPresenter;
import com.extra4it.adsapp.ui.login.LoginMvpView;
import com.extra4it.adsapp.ui.login.LoginPresenter;
import com.extra4it.adsapp.ui.main.MainMvpPresenter;
import com.extra4it.adsapp.ui.main.MainMvpView;
import com.extra4it.adsapp.ui.main.MainPresenter;
import com.extra4it.adsapp.ui.myAds.MyAdsMvpPresenter;
import com.extra4it.adsapp.ui.myAds.MyAdsMvpView;
import com.extra4it.adsapp.ui.myAds.MyAdsPresenter;
import com.extra4it.adsapp.ui.notifications.NotificationMvpPresenter;
import com.extra4it.adsapp.ui.notifications.NotificationMvpView;
import com.extra4it.adsapp.ui.notifications.NotificationPresenter;
import com.extra4it.adsapp.ui.notifications.NotificationsAdapter;
import com.extra4it.adsapp.ui.profile.ProfileMvpPresenter;
import com.extra4it.adsapp.ui.profile.ProfileMvpView;
import com.extra4it.adsapp.ui.profile.ProfilePresenter;
import com.extra4it.adsapp.ui.rating.RatingMvpPresenter;
import com.extra4it.adsapp.ui.rating.RatingMvpView;
import com.extra4it.adsapp.ui.rating.RatingPresenter;
import com.extra4it.adsapp.ui.recharge.RechargeMvpPresenter;
import com.extra4it.adsapp.ui.recharge.RechargeMvpView;
import com.extra4it.adsapp.ui.recharge.RechargePresenter;
import com.extra4it.adsapp.ui.register.RegisterMvpPresenter;
import com.extra4it.adsapp.ui.register.RegisterMvpView;
import com.extra4it.adsapp.ui.register.RegisterPresenter;
import com.extra4it.adsapp.ui.register.city.CityMvpPresenter;
import com.extra4it.adsapp.ui.register.city.CityMvpView;
import com.extra4it.adsapp.ui.register.city.CityPresenter;
import com.extra4it.adsapp.ui.report.ReportMvpPresenter;
import com.extra4it.adsapp.ui.report.ReportMvpView;
import com.extra4it.adsapp.ui.report.ReportPresenter;
import com.extra4it.adsapp.ui.retrievePassword.RetrievePasswordMvpPresenter;
import com.extra4it.adsapp.ui.retrievePassword.RetrievePasswordMvpView;
import com.extra4it.adsapp.ui.retrievePassword.RetrievePasswordPresenter;
import com.extra4it.adsapp.ui.search.SearchMvpPresenter;
import com.extra4it.adsapp.ui.search.SearchMvpView;
import com.extra4it.adsapp.ui.search.SearchPresenter;
import com.extra4it.adsapp.ui.searchResult.SearchResultMvpPresenter;
import com.extra4it.adsapp.ui.searchResult.SearchResultMvpView;
import com.extra4it.adsapp.ui.searchResult.SearchResultPresenter;
import com.extra4it.adsapp.ui.setting.SettingMvpPresenter;
import com.extra4it.adsapp.ui.setting.SettingMvpView;
import com.extra4it.adsapp.ui.setting.SettingPresenter;
import com.extra4it.adsapp.ui.splash.SplashMvpPresenter;
import com.extra4it.adsapp.ui.splash.SplashMvpView;
import com.extra4it.adsapp.ui.splash.SplashPresenter;
import com.extra4it.adsapp.ui.user.UserAdsAdapter;
import com.extra4it.adsapp.ui.user.UserMvpPresenter;
import com.extra4it.adsapp.ui.user.UserMvpView;
import com.extra4it.adsapp.ui.user.UserPresenter;
import com.extra4it.adsapp.utils.PickImage;
import com.extra4it.adsapp.utils.RunTimePermission;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * Created by user on 09/05/2018.
 */

@Module
public class ActivityModule {
    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideAppCompatActivity() {
        return mActivity;
    }

    @Provides
    SplashMvpPresenter<SplashMvpView> splashMvpPresenter(SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    IntroMvpPresenter<IntroMvpView> introMvpPresenter(IntroPresenter<IntroMvpView> presenter) {
        return presenter;
    }

    @Provides
    LoginMvpPresenter<LoginMvpView> loginMvpPresenter(LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    RegisterMvpPresenter<RegisterMvpView> registerMvpPresenter(RegisterPresenter<RegisterMvpView> presenter) {
        return presenter;
    }

    @Provides
    RetrievePasswordMvpPresenter<RetrievePasswordMvpView> retrievePasswordMvpPresenter(RetrievePasswordPresenter<RetrievePasswordMvpView> presenter) {
        return presenter;
    }

    @Provides
    MainMvpPresenter<MainMvpView> mainMvpPresenter(MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    HomeMvpPresenter<HomeMvpView> homeMvpPresenter(HomePresenter<HomeMvpView> presenter) {
        return presenter;
    }

    @Provides
    BrandMvpPresenter<BrandMvpView> brandMvpPresenter(BrandPresenter<BrandMvpView> presenter) {
        return presenter;
    }

    @Provides
    CitySheetMvpPresenter<CitySheetMvpView> citySheetMvpPresenter(CitySheetPresenter<CitySheetMvpView> presenter) {
        return presenter;
    }

    @Provides
    FavMvpPresenter<FavMvpView> favMvpPresenter(FavPresenter<FavMvpView> presenter) {
        return presenter;
    }

    @Provides
    BankAccountMvpPresenter<BankAccountMvpView> bankAccountMvpPresenter(BankAccountPresenter<BankAccountMvpView> presenter) {
        return presenter;
    }

    @Provides
    ForbiddenMvpPresenter<ForbiddenMvpView> forbiddenMvpPresenter(ForbiddenPresenter<ForbiddenMvpView> presenter) {
        return presenter;
    }

    @Provides
    ConditionsMvpPresenter<ConditionsMvpView> conditionsMvpPresenter(ConditionsPresenter<ConditionsMvpView> presenter) {
        return presenter;
    }

    @Provides
    AdvMvpPresenter<AdvMvpView> advMvpPresenter(AdvPresenter<AdvMvpView> presenter) {
        return presenter;
    }

    @Provides
    ReportMvpPresenter<ReportMvpView> reportMvpPresenter(ReportPresenter<ReportMvpView> presenter) {
        return presenter;
    }

    @Provides
    RatingMvpPresenter<RatingMvpView> ratingMvpPresenter(RatingPresenter<RatingMvpView> presenter) {
        return presenter;
    }

    @Provides
    LikesMvpPresenter<LikesMvpView> likesMvpPresenter(LikesPresenter<LikesMvpView> presenter) {
        return presenter;
    }

    @Provides
    SearchMvpPresenter<SearchMvpView> searchMvpPresenter(SearchPresenter<SearchMvpView> presenter) {
        return presenter;
    }

    @Provides
    SearchResultMvpPresenter<SearchResultMvpView> searchResultMvpPresenter(SearchResultPresenter<SearchResultMvpView> presenter) {
        return presenter;
    }

    @Provides
    ContactMvpPresenter<ContactMvpView> contactMvpPresenter(ContactPresenter<ContactMvpView> presenter) {
        return presenter;
    }

    @Provides
    ProfileMvpPresenter<ProfileMvpView> profileMvpPresenter(ProfilePresenter<ProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    MyAdsMvpPresenter<MyAdsMvpView> myAdsMvpPresenter(MyAdsPresenter<MyAdsMvpView> presenter) {
        return presenter;
    }

    @Provides
    SettingMvpPresenter<SettingMvpView> settingMvpPresenter(SettingPresenter<SettingMvpView> presenter) {
        return presenter;
    }

    @Provides
    EditInformationMvpPresenter<EditInformationMvpView> editInformationMvpPresenter(EditInformationPresenter<EditInformationMvpView> presenter) {
        return presenter;
    }

    @Provides
    ChangePhoneMvpPresenter<ChangePhoneMvpView> changePhoneMvpPresenter(ChangePhonePresenter<ChangePhoneMvpView> presenter) {
        return presenter;
    }

    @Provides
    ChangePasswordMvpPresenter<ChangePasswordMvpView> changePasswordMvpPresenter(ChangePasswordPresenter<ChangePasswordMvpView> presenter) {
        return presenter;
    }

    @Provides
    RechargeMvpPresenter<RechargeMvpView> rechargeMvpPresenter(RechargePresenter<RechargeMvpView> presenter) {
        return presenter;
    }

    @Provides
    ConfirmPayMvpPresenter<ConfirmPayMvpView> confirmPayMvpPresenter(ConfirmPayPresenter<ConfirmPayMvpView> presenter) {
        return presenter;
    }

    @Provides
    NotificationMvpPresenter<NotificationMvpView> notificationMvpPresenter(NotificationPresenter<NotificationMvpView> presenter) {
        return presenter;
    }

    @Provides
    UserMvpPresenter<UserMvpView> userMvpPresenter(UserPresenter<UserMvpView> presenter) {
        return presenter;
    }

    @Provides
    CityMvpPresenter<CityMvpView> cityMvpPresenter(CityPresenter<CityMvpView> presenter) {
        return presenter;
    }

    @Provides
    AddMvpPresenter<AddMvpView> addPresenter(AddPresenter<AddMvpView> presenter) {
        return presenter;
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager() {
        return new LinearLayoutManager(mActivity);
    }

    @Provides
    NotificationsAdapter provideNotificationAdapter() {
        return new NotificationsAdapter(mActivity, new ArrayList<NotificationsResponse>());
    }

    @Provides
    UserAdsAdapter provideUserAdsAdapter() {
        return new UserAdsAdapter(mActivity, new ArrayList<BrandResponse>());
    }

    @Provides
    RunTimePermission getRunTimePermission() {
        return new RunTimePermission(mActivity);
    }

    @Provides
    PickImage getPicImage() {
        return new PickImage(mActivity);
    }
}
