package com.extra4it.adsapp.di.module;


import android.app.Application;
import android.content.Context;

import com.extra4it.adsapp.data.AppDataManager;
import com.extra4it.adsapp.data.DataManager;
import com.extra4it.adsapp.data.network.ApiHelper;
import com.extra4it.adsapp.data.prefs.AppPreferencesHelper;
import com.extra4it.adsapp.data.prefs.PreferencesHelper;
import com.extra4it.adsapp.di.ApplicationContext;
import com.extra4it.adsapp.di.PreferencesInfo;
import com.extra4it.adsapp.utils.AppConstants;

import java.util.Collections;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 09/05/2018.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;
    private final String mBaseUrl;

    public ApplicationModule(Application application, String baseUrl) {
        this.mApplication = application;
        this.mBaseUrl = baseUrl;
    }

    @ApplicationContext
    @Provides
    Context ApplicationContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Singleton
    @Provides
    DataManager provideDataManager(AppDataManager AppDataManager) {
        return AppDataManager;
    }

    // Shared Preferences
    @Provides
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @PreferencesInfo
    @Provides
    String preferencesName() {
        return AppConstants.PREF_NAME;
    }

    // retrofit
    @Provides
    @Singleton
    Cache provideHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient(Cache cache) {
        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                .build();
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.cache(cache);
        client.connectionSpecs(Collections.singletonList(spec));
        return client.build();
    }

    @Provides
    HttpLoggingInterceptor provideHttpLogging() {
        return new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    OkHttpClient.Builder provideBuilder() {
        return new OkHttpClient.Builder();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(OkHttpClient.Builder builder, HttpLoggingInterceptor interceptor) {
        return new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .client(builder.addInterceptor(interceptor).build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    ApiHelper provideApiHelper(Retrofit retrofit) {
        return retrofit.create(ApiHelper.class);
    }


}
