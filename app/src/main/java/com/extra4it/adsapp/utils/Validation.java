package com.extra4it.adsapp.utils;

import android.content.Context;
import android.widget.EditText;

import com.extra4it.adsapp.R;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 01/04/2018.
 */

public class Validation {

    public static boolean internetConnectionAvailable() {
        InetAddress inetAddress = null;
        try {
            Future<InetAddress> future = Executors.newSingleThreadExecutor().submit(new Callable<InetAddress>() {
                @Override
                public InetAddress call() {
                    try {
                        return InetAddress.getByName("google.com");
                    } catch (UnknownHostException e) {
                        return null;
                    }
                }
            });
            inetAddress = future.get(10, TimeUnit.SECONDS);
            future.cancel(true);
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
        } catch (TimeoutException e) {
        }
        return inetAddress != null && !inetAddress.equals("");
    }

    public static Boolean isPhoneValid(Context context, EditText phone) {
        boolean isValid = false;
        if (phone.length() == 11) {
            String[] x = phone.getText().toString().split("");
            if (x[1].equals("0") && x[2].equals("1")) {
                if (x[3].equals("1") || x[3].equals("0") || x[3].equals("2") || x[3].equals("5")) {
                    isValid = true;
                }
            }
        }
        return isValid;
    }

    public static boolean isEmailValid(Context context, EditText email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email.getText().toString();

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        } else {
            email.setError(context.getResources().getString(R.string.mail_validation_error));

        }
        return isValid;
    }

    public static boolean isPasswordValid(Context context, EditText password) {
        boolean isValid = false;
        if (password.length() > 5) {
            isValid = true;
        } else {
            password.setError(context.getResources().getString(R.string.password_validation));
        }
        return isValid;
    }

    public static boolean isPasswordMatch(Context context, EditText password, EditText rePassword) {
        boolean isValid = false;

        if (password.getText().toString().equals(rePassword.getText().toString())) {
            isValid = true;
        } else {
            password.setError(context.getResources().getString(R.string.not_matched));
            rePassword.setError(context.getResources().getString(R.string.not_matched));
        }
        return isValid;
    }

    public static boolean validate(Context context, EditText[] fields) {
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];
            if (currentField.getText().toString().length() <= 0) {
                currentField.setError(context.getResources().getString(R.string.required_field));
                return false;
            }
        }
        return true;
    }
}
