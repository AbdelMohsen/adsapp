package com.extra4it.adsapp.utils;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import com.extra4it.adsapp.ui.base.BasePresenter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.extra4it.adsapp.utils.AppConstants.CAMERA_REQUEST_RESULT_CODE;
import static com.extra4it.adsapp.utils.AppConstants.IMAGE_REQUEST_RESULT_CODE;


public class PickImage {
    private Activity context;

    @Inject
    public PickImage(Activity context) {
        this.context = context;
    }

    // open gallery to pick image
    public void openGallery() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");

        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");

        Intent chooserIntent = Intent.createChooser(getIntent, "choose Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
        context.startActivityForResult(chooserIntent, IMAGE_REQUEST_RESULT_CODE);
    }

    public String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivityForResult(takePictureIntent, CAMERA_REQUEST_RESULT_CODE);
        }
    }

    public void onActivityResult(BasePresenter basePresenter, int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_REQUEST_RESULT_CODE) {

                Uri uri = data.getData();
                String realPathFromURIPath = null;
                File file = null;
                if (uri != null) {
                    realPathFromURIPath = getRealPathFromURIPath(uri, context);
                    file = new File(realPathFromURIPath);
                    RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);

                }
                Bitmap bitmap;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                    if (basePresenter instanceof ChooseImageCallBack) {
                        ((ChooseImageCallBack) basePresenter).onChooseImage(uri, realPathFromURIPath, file, bitmap);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CAMERA_REQUEST_RESULT_CODE) {
                if (data.hasExtra("data")) {


                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    File file = convertBitmapToFile(bitmap, "img");

                    if (basePresenter instanceof ChooseImageCallBack) {
                        ((ChooseImageCallBack) basePresenter).onChooseImage(null,file.getName(), file, bitmap);
                    }
                }
            }
        }

    }

    private File convertBitmapToFile(Bitmap bitmap, String name) {
        File filesDir = context.getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }

    public interface ChooseImageCallBack {
        void onChooseImage(Uri uri, String realPath, File file, Bitmap bitmap);
    }

}
