package com.extra4it.adsapp.utils;

/**
 * Created by user on 07/05/2018.
 */

public final class AppConstants {

    public static final int NULL_INDEX = -1;
    public static final String STRING_NULL_INDEX = null;
    public static final String PREF_NAME = "app_pref_name";
    public static final String BASE_URL = "http://2.extra4it.net/adsapp/api/";
    public static final String BASE_IMAGE_URL = "http://2.extra4it.net/adsapp/storage/app/public/";
    public static final int IMAGE_REQUEST_CODE = 2;
    public static final int CAMERA_REQUEST_CODE = 3;
    public static final int IMAGE_REQUEST_RESULT_CODE = 102;
    public static final int CAMERA_REQUEST_RESULT_CODE = 103;


}
