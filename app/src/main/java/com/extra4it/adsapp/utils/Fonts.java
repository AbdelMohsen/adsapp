package com.extra4it.adsapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by user on 12/03/2018.
 */

public class Fonts {
    public static void setFont(Button[] buttons, Context context, int fontIndex) {

        for (Button button : buttons) {
            Typeface font = null;
            switch (fontIndex) {
                case 1:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Regular.ttf");
                    break;
                case 2:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Bold.ttf");
                    break;
            }
            button.setTypeface(font);
        }
    }

    public static void setFont(TextView[] textViews, Context context, int fontIndex) {

        for (TextView textView : textViews) {
            Typeface font = null;
            switch (fontIndex) {
                case 1:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Regular.ttf");
                    break;
                case 2:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Bold.ttf");
                    break;
            }
            textView.setTypeface(font);
        }
    }

    public static void setFont(EditText[] editTexts, Context context, int fontIndex) {

        for (TextView editText : editTexts) {
            Typeface font = null;
            switch (fontIndex) {
                case 1:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Regular.ttf");
                    break;
                case 2:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Bold.ttf");
                    break;
            }
            editText.setTypeface(font);
        }
    }

    public static void setFont(RadioButton[] radioButtons, Context context, int fontIndex) {
        for (RadioButton radioButton : radioButtons) {
            Typeface font = null;
            switch (fontIndex) {
                case 1:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Regular.ttf");
                    break;
                case 2:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Bold.ttf");
                    break;
            }

            radioButton.setTypeface(font);
        }
    }

    public static void setFont(TextInputLayout[] textInputLayouts, Context context, int fontIndex) {
        for (TextInputLayout textInputLayout : textInputLayouts) {
            Typeface font = null;
            switch (fontIndex) {
                case 1:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Regular.ttf");
                    break;
                case 2:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Bold.ttf");
                    break;
            }

            textInputLayout.setTypeface(font);
        }
    }

    public static void setFont(CheckBox[] checkBoxes, Context context, int fontIndex) {
        for (CheckBox checkBox : checkBoxes) {
            Typeface font = null;
            switch (fontIndex) {
                case 1:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Regular.ttf");
                    break;
                case 2:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Bold.ttf");
                    break;
            }

            checkBox.setTypeface(font);
        }
    }

    public static void setFont(AutoCompleteTextView[] autoCompleteTextViews, Context context, int fontIndex) {
        for (AutoCompleteTextView autoCompleteTextView : autoCompleteTextViews) {
            Typeface font = null;
            switch (fontIndex) {
                case 1:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Regular.ttf");
                    break;
                case 2:
                    font = Typeface.createFromAsset(context.getResources().getAssets(), "Cairo-Bold.ttf");
                    break;
            }

            autoCompleteTextView.setTypeface(font);
        }
    }
}
