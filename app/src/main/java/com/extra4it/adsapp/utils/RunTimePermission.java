package com.extra4it.adsapp.utils;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import javax.inject.Inject;

import static com.extra4it.adsapp.utils.AppConstants.CAMERA_REQUEST_CODE;
import static com.extra4it.adsapp.utils.AppConstants.IMAGE_REQUEST_CODE;

public class RunTimePermission {

    private static final String TAG = "xxx";
    private Activity context;

    @Inject
    public RunTimePermission(Activity context) {
        this.context = context;
    }

    public void requestImageFromGalleryPermission() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // show explanation id permission denied
            if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                    ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("permission needed");
                builder.setMessage("please approve permission to make app access photos");
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, IMAGE_REQUEST_CODE);
                    }
                });
                builder.create().show();
            } else {
                // permission granted
                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, IMAGE_REQUEST_CODE);

            }
        } else {
            //Permission granted
            Log.e("xxx", "permission granted already");
            PickImage pickImage = new PickImage(context);
            pickImage.openGallery();
        }

    }


    public void requestImageFromCameraPermission() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // show explanation id permission denied
            if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.CAMERA)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("permission needed");
                builder.setMessage("please approve permission to make app access camera");
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CAMERA},
                                CAMERA_REQUEST_CODE);
                    }
                });
            } else {
                // permission granted
                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);


            }
        } else {
            //Permission granted
            Log.e("xxx", "permission granted already");
            new PickImage(context).openCamera();
        }

    }

    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    requestImageFromCameraPermission();
                } else if (items[item].equals("Choose from Library")) {
                    requestImageFromGalleryPermission();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


}
