package com.extra4it.adsapp.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by user on 19/05/2018.
 */

public class CommonUtils {

    public static void hideProgressBar(android.widget.ProgressBar progressBar, Activity context) {
        progressBar.setVisibility(View.GONE);
        if (context != null) {
            context.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }

    }

    public static void showProgressBar(android.widget.ProgressBar progressBar, Activity context) {
        progressBar.setVisibility(View.VISIBLE);
        if (context != null) {
            context.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    // hide Keyboard
    public static void hideKeyboard(Activity context) {
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            }
        }
    }

    // show Keyboard
    public static void showKeyboard(EditText editText, Context context) {
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
    }
}
